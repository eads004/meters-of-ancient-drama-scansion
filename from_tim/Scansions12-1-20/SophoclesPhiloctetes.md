Sophocles *Philoctetes* Scansion

Prepared by Timothy Moore

Text and Scansion: *Sophocles Philoctetes*, edd. Seth L. Schein,
Cambridge: Cambridge University Press, 2013

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-134**

**PARODOS: 135-218**

*135-218: Epirrhematic Amoibaion; chorus lyrics (lyric, with responding
stanzas), Neoptolemus anapests (nonlyric), lyric 201=211*

**FIRST EPISODE: 219-675**

218: extrametric: ἰὼ ξένοι: Philoctetes

219-390: ia3: nonlyric

*391-402: choral strophe (=507-515): lyrics (lyric)*; 393 ia3 (lyric)

403-506: ia3 (nonlyric)

antilabe: 466

*507-515: choral antistrophe (=391-402): lyrics (lyric)*; 509 ia3
(lyric)

516-675: ia3 (nonlyric)

antilabe: 589-590

antilabe: 674

**FIRST STASIMON: 676-729**

*676-729: lyrics (lyric, includes responding stanzas)*

**SECOND EPISODE: 730-826**

736-826: ia3 (nonlyric)

732: extrametric: ἆ ἆ ἆ ἆ.: Philoctetes

antilabe: 733

736: extrametric: ἰὼ θεοί.: Philoctetes

739: extrametric: ἆ ἆ ἆ ἆ.: Philoctetes

750: extrametric: ἴθ᾽ ῶ παῖ.: Philoctetes

antilabe: 753-754

antilabe: 757

antilabe: 759

Schein has no 761

782: extrametric: ἆ ἆ ἆ ἆ.: Philoctetes

Schein has a 782a after 782

785: extrametric: παπαῖ, φεῦ.: Philoctetes

787: extrametric: προσέρπει.: Philoctetes

790: extrametric: ἀτταταῖ.: Philoctetes

796: extrametric: ὤμοι μοι.: Philoctetes

804: extrametric: τί φήις, παῖ.: Philoctetes

antilabe: 810

antilabe: 813-814

antilabe: 816-817

**SECOND STASIMON: 827-864**

*827-838: lyrics (lyric: strophe; chorus)*

STROPHE: 827-838

*839-842: dac6 (nonlyric?; Neoptolemus; we need Philoctetes)*

*843-864: lyrics (lyric, includes responding stanzas, chorus)*

**THIRD EPISODE: 865-1080**

865-1080: ia3 (nonlyric)

antilabe: 917

antilabe: 974

antilabe: 981

antilabe: 985

antilabe: 994

antilabe: 1001

1018a: extrametric: φεῦ.: Philoctetes

**AMOIBAION: 1081-1217**

*1081-1217: Lyric amoibaion: Philoctetes and chorus lyrics (including
responding stanzas)*

**EXODOS: 1218-1420 (=4^th^ Episode)**

1218-1401: ia3 (nonlyric)

antilabe: 1248

Schein has 1251a and 1251b, with a lacuna, after 1250

antilabe: 1254-1255

antilabe: 1275

antilabe: 1277

antilabe: 1280

antilabe: 1286

antilabe: 1296

antilabe: 1302

Schein has 1365a and 1365b after 1365

*1402-1408: tr4\^* (nonlyric, Neoptolemus and Philoctetes prepare to
leave)

*1409-1417: anapests (nonlyric; Heracles stops Neoptolemus and
Philoctetes)*

1418-1444: ia3 (nonlyric, Heracles’ speech)

*1445-1471: anapests (nonlyric, Philoctetes agrees to go and says
farewell; choral tag)*
