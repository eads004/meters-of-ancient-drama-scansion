Sophocles *Oedipus at Colonus* Scansion

Prepared by Timothy Moore

Text: *Sophoclis Fabulae*, edd. H. Lloyd-Jones and N.G. Wilson, Oxford:
Clarendon Press, 1990.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-116**

1-116: ia3 (nonlyric)

**PARODOS: 117-253**

*117-253: Epirrhematic amoibaion, then lyric amoibaion: chorus lyrics
(with responding stanzas117-207, astrophic 206-253), with some anapests
(nonlyric?); Oedipus and Antigone anapests (nonlyric?), then lyrics
(lyric)*

**FIRST EPISODE: 254-509**

254-509: ia3 (nonlyric)

315: extrametric: τί φωνῶ;: Oedipus

318: extrametric: τάλαινα,: Oedipus

antilabe: 330, 331, 332, 333

**AMOIBAION: 510-548**

*510-548: Lyric Amoibaion: Chorus and Oedipus lyrics (lyric, with
responding stanzas);* ia3 539=546 (nonlyric? questioning)

**SECOND EPISODE: 549-667**

549-667: ia3 (nonlyric)

antilabe: 652, 653, 654, 655, 666

**FIRST STASIMON: 668-719**

*668-719: lyrics (lyric, includes responding stanzas)*

**THIRD EPISODE: 720-1043**

720-832: ia3 (nonlyric)

antilabe: 722

antilabe: 820, 821

antilabe: 831, 832

*833-886: Epirrhematic amoibaion: chorus and Oedipus lyrics (lyric, with
responding stanzas)* and ia3 (nonlyric?); Creon ia3 (nonlyric), Antigone
ia3 (nonlyric?)

*887-890: tr4\^ (nonlyric, Theseus)*

891-1043: ia3 (nonlyric)

antilabe: 896

Lloyd-Jones and Wilson bracket 954-955

Lloyd-Jones and Wilson place 1028-1033 after 1019

**SECOND STASIMON: 1044-1095**

*1044-1096: lyrics (lyric, with responding stanzas)*

**FOURTH EPISODE: 1096-1210**

1096-1210: ia3 (nonlyric)

antilabe: 1106, 1107. 1108, 1109

antilabe: 1169, 1170

**THIRD STASIMON: 1211-1248**

*1211-1248: lyrics (lyric, includes responding stanzas)*

**FIFTH EPISODE: 1249-1555**

antilabe: 1252

1271: extrametric: τί σιγᾷς;: Polyneices

Lloyd-Jones and Wilson bracket 1300

Lloyd-Jones and Wilson bracket1436

antilabe: 1441, 1442, 1443

*1447-1499: Epirrhematic Amoibaion: chorus lyrics (lyric, with
responding stanzas)*; Oedipus and Antigone ia3 (nonlyric)

1500-1555: ia3 (nonlyric)

**FOURTH STASIMON: 1556-1578**

*1556-1578: lyrics (lyric, includes responding stanzas)*

**EXODOS: 1579-1779 (=6^th^ Episode)**

1579-1669: ia3 (nonlyric)

antilabe: 1583

*1670-1750: amoibaion (with responding stanzas); chorus lyrics (lyric)*;
ia3 1678=1705 (nonlyric?), 1737 (nonlyric?); *Antigone and Ismene lyrics
(lyric*); ia3 1678=1705 (nonlyric?); 1724=1737 (nonlyric)

*1751-1779: anapests (nonlyric)*
