**Aeschylus, *Persians***

Prepared by Timothy Moore.

Text, scansion, and line numbering: West, M. L., ed. 1990. *Aeschyli
tragoediae: cum incerti poetae Prometheo*. Bibliotheca Scriptorum
Graecorum et Romanorum Teubneriana. Stuttgart: Teubner.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**Parodos: 1-154**

*1-64: anapests (nonlyric)*

*65-139: lyrics (includes responding stanzas)*

*140-154: anapests (nonlyric)*

**1^st^ Episode: 155-531**

*155-175: tr4\^ (nonlyric)*

176-214: ia3 (nonlyric: report of dream)

*215-248: tr4\^ (nonlyric)*

249-255: ia3 (nonlyric: messenger report)

*256-289: epirrhematic amoibaion; chorus lyrics (with responding
stanzas),* messenger ia3 (nonlyric)

290-531: ia3 (nonlyric)

**1^st^ Stasimon: 532-597**

*532-547: anapests (nonlyric)*

*548-597: lyrics (includes responding stanzas)*

**2^nd^ Episode: 598-622**\
598-622: ia3 (nonlyric; Queen rhesis)

**2^nd^ Stasimon: 623-680**\
*623-632: anapests (nonlyric; chorus calls upon ghost of Darius)*

*633-680: lyrics (includes responding stanzas)*

**3^rd^ Episode: 681-851**

681-693: ia3 (nonlyric; Darius)

*694-703: epirrhematic amoibaion; chorus lyrics (with responding
stanzas), Darius tr4\^ (nonlyric)*

*703-758: tr4\^ (nonlyric; Darius and Queen)*

759-851: ia3 (nonlyric)

**3^rd^ Stasimon: 852-907**

*852-907: lyrics (includes responding stanzas)*

**Exodos: 908-1077 (=4^th^ episode)**

*908-921: anapests (nonlyric; Xerxes’ entrance and chorus’ response)*

*922-931: anapests (lyric? West calls this a proode and notes
significance of clausula at 930/1)*

*932-1075: lyrics (includes responding stanzas; amoibaion between Xerxes
and chorus; includes many lyric anapests through 998, then none)*
