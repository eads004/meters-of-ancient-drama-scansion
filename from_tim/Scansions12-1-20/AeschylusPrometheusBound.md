**Aeschylus, *Prometheus Bound***

Prepared by Timothy Moore.

Text, scansion, and line numbering: West, M. L., ed. 1990. *Aeschyli
tragoediae: cum incerti poetae Prometheo*. Bibliotheca Scriptorum
Graecorum et Romanorum Teubneriana. Stuttgart: Teubner.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**Prologue: 1-127**

1-87: ia3 (nonlyric)

*93-100: anapests (nonlyric; within Prometheus’ speech, like all the
following through 127)*

101-113: ia3 (nonlyric)

*114-115: lyrics (lyric)*

116: ia3 (nonlyric?)

*117: lyrics (lyric)*

118-110: ia3 (nonlyric?)

*120-127: anapests (nonlyric?)*

**Parodos: 128-192**

*128-192: epirrhematic amoibaion: chorus lyrics (with responding
stanzas), Prometheus anapests (nonlyric?)*

**1^st^ Episode: 193-396**

193-276: ia3 (nonlyric)

*277-297: anapests (nonlyric; chorus dismounting, entrance of Okeanos)*

298-396: ia3 (nonlyric)

**1^st^ Stasimon: 397-435**

*397-435: lyrics (lyric; includes responding stanzas)*

**2^nd^ Episode: 436-525**

436-525: ia3 (nonlyric)

**2^nd^ Stasimon: 526-560**

*526-560: lyrics (lyric; includes responding stanzas)*

**3^rd^ Episode: 561-886**

*561-565: anapests (nonlyric; Io entrance)*

*566-608: epirrhematic amoibaion lyrics (lyric; includes responding
stanzas (574-588 and 593-608) for Io,* ia3 (nonlyric) for Prometheus

609-686: ia3 (nonlyric)

*687-695: lyrics (lyric, astrophic)*

696-876: ia3 (nonlyric)

742: extra metric (Io: ἰώ μοί μοι, ἐἕ)

Wests proposes lacuna making 860 two lines

*877-886: anapests (nonlyric?; Io)*

**3^rd^ Stasimon: 887-906**

*887-906: lyrics (lyric; includes responding stanzas)*; 901 ia3 (lyric?)

**Exodos: 907-1093 (=4^th^ Episode)**

907-1039: ia3 (nonlyric)

West proposes lacuna of one verse after 969

*1040-1093: an (nonlyric)*
