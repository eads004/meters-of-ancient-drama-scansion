Aristophanes, *Knights*[^1]

Prepared by Timothy Moore

(Text and Scansion : L.P.E. Parker, *The Songs of Aristophanes*
\[Oxford: Clarendon Press, 1997\] where available. Elsewhere
*Aristophanis Fabulae*, ed. N.G. Wilson \[Oxford: Clarendon Press,
2007\])

**Prologue: 1-274**

*1-274: ia2 nonlyric*

1 extrametric: *iou, iou*, Strepsiades

[^1]: Text Wilson unless otherwise noted. Scansion Parker 1997 where it
    is available (unless otherwise noted), otherwise Wilson, with
    Anderson and Dix.
