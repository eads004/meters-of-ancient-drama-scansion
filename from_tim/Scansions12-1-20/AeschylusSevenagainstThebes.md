**Aeschylus, *Seven against Thebes***

Prepared by Timothy Moore.

Text, scansion, and line numbering: West, M. L., ed. 1990. *Aeschyli
tragoediae: cum incerti poetae Prometheo*. Bibliotheca Scriptorum
Graecorum et Romanorum Teubneriana. Stuttgart: Teubner.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**Prologue: 1-77**

1-77: ia3 (nonlyric; Eteocles rhesis, messenger)

**Parodos: 78-181**

*78-181: lyrics (lyric; includes responding stanzas (108-181)*

**1^st^ Episode: 182-286**

182-202: ia3 (nonlyric; Eteocles)

*203-244: epirrhematic amoibaion: chorus lyrics,* Eteocles ia3
(nonlyric)

245-286: ia3 (nonlyric: chorus responds with ia3 at 245)

**1^st^ Stasimon: 287-368**

*287-368: lyrics (lyric; includes responding stanzas (304-369))*

**2^nd^ Episode: 369-719**

369-416: ia3 (nonlyric, Messenger and Eteocles)

*417-630: epirrhematic amoibaion: chorus lyrics (with responding
stanzas),* Messenger and Eteocles ia3 (nonlyric)

631-685: ia3 (nonlyric: chorus and Eteocles)

*686-711: epirrhematic amoibaion: chorus lyrics (with responding
stanzas),* Eteocles ia3 (nonlyric)

712-719: ia3 (nonlyric: chorus and Eteocles)

**2^nd^ Stasimon: 720-791**

*720-791: lyrics (lyric; includes responding stanzas)*

**3^rd^ Episode: 792-821**

792-821: ia3 (nonlyric, Messenger and chorus)

**3^rd^ Stasimon: 822-860**

*822-831: anapests (nonlyric; chorus, should I rejoice or mourn?; starts
address to Zeus)*

*832-860: (lyric; includes responding stanzas* and ia3s (nonlyric?)
within mesode (848-851, 855-6, 859))

**Exodos: 861-1078 (=4^th^ episode)**

*\[861-873: anapests (nonlyric; chorus hears Antigone and Ismene: West
brackets)\]*

*874-1004: lyrics (lyric; includes responding stanzas*, and ia3s
(nonlyric?, 875, 881, 961, 988)

\[1005-1058: ia3 (nonlyric; Herald and Antigone, West brackets)\]

*\[1059-1077: anapests (nonlyric; divides in two, one with P., one with
E.; West brackets)\]*
