Euripides, *Electra*

Prepared by Timothy Moore, Shangwei Deng, and Zixing Chen

Text: *Euripidis Fabulae*, vol. 2, ed. J. Diggle (Oxford: Clarendon
Press: 1981 \[repr. with corrections 1986\]).\
Scansion of lyrics: Frederico Lourenço, *The Lyric Metres of Euripidean
Drama* (Coimbra: Centro de Estudos Clássicos e Humanisticos da
Universidade de Coimbra, 2011).

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**Prologue: 1-111**

1-111: ia3: nonlyric

[]{#_Hlk46904171 .anchor}Diggle places line 59 (ϲύνοιδέ μοι Κύπριϲ)
between line 56 and line 57.

*112-166 : lyrics (lyric, includes responding stanzas, Electra’s
monody)*

extrametric: 114 io moi moi, Electra

extrametric: 129 io moi moi, Electra

STROPHE 1: 112-124

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

Ηλ.‎ ϲύντειν‎ʼ (ὥρα‎) |ποδὸϲ ὁρμάν·‎ ὤ,‎ 112 an2||H

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒ ∩

ἔμβα ἔμβα| κατακλαίουϲα.‎ 113 an2||BH

⏑‒ ‒ ‒

ἰώ μοί μοι.‎ 114 e.m.?

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἐγενόμαν Ἀγαμέμνονοϲ‎ 115 gly

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

καί μʼ ἔτικτε Κλυταιμήϲτρα‎ 116 gly, dragged

‒ ‒ ‒ ⏑ ⏑‒ ⏑ ‒

ϲτυγνὰ Τυνδάρεω κόρα,‎ 117 gly

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑‒

κικλήϲκουϲι δέ μʼ ἀθλίαν‎ 118 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

Ἠλέκτραν πολιῆται.‎ 119 pher||

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

φεῦ φεῦ ϲχετλίων πόνων‎ 120 tel

‒ ⏑ ⏑ ‒ ⏑ ‒

καὶ ϲτυγερᾶϲ ζόαϲ.‎ 121 dod||

‒ ⏑ ‒ ⏑ ⏑ ‒⏑‒

ὦ πάτερ, ϲὺ δʼ ἐν Ἀίδα‎ 122 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

κεῖϲαι ϲᾶϲ ἀλόχου ϲφαγαῖϲ‎ 123 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

Αἰγίϲθου τʼ,‎ Ἀγάμεμνον.‎ 124 pher

MESODE 1: 125-126

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ⏑

‎ἴθι τὸν αὐτὸν ἔγειρε γόον,‎ 125 gly?

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἄναγε πολύδακρυν ἁδονάν.‎ 126 ia2 (gly?)

ANTISTROPHE 1: 127-139

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

‎ϲύντειν‎ʼ (ὥρα‎) |ποδὸϲ ὁρμάν·‎ ὤ,‎ 127 an2||H

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒ ∩

ἔμβα ἔμβα |κατακλαίουϲα.‎ 128 an2||BH

⏑‒ ‒ ‒

ἰώ μοί μοι.‎ 129 e.m.?[^1]

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

τίνα πόλιν, τίνα δʼ οἶκον, ὦ‎ 130 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒ ‒

τλᾶμον ϲύγγονʼ,‎ ἀλατεύειϲ‎ 131 gly, dragged

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

οἰκτρὰν ἐν θαλάμοιϲ λιπὼν‎ 132 gly

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

πατρώιοιϲ ἐπὶ ϲυμφοραῖϲ‎ 133 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ἀλγίϲταιϲιν ἀδελφάν;‎ 134 pher||

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἔλθοιϲ δὲ πόνων ἐμοὶ‎ 135 tel

‒ ⏑ ⏑‒ ⏑ ‒

τᾶι μελέαι λυτήρ,‎ 136 dod||

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ὦ Ζεῦ Ζεῦ, πατρί θʼ αἱμάτων‎ 137 gly

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

αἰϲχίϲτων ἐπίκουροϲ, Ἄρ-‎ 138 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

γει κέλϲαϲ πόδʼ ἀλάταν.‎ 139 pher

STROPHE 2: 140-149

‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑

‎θὲϲ τόδε |τεῦχοϲ ἐ|μᾶϲ ἀπὸ |κρατὸϲ ἑ-‎ 140 dac4∫

‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑ |‒

λοῦϲʼ,‎ ἵνα| πατρὶ γό|ουϲ νυχί|ουϲ‎ 141 dac4\^

⏑ ‒ ⏑ ⏑ ‒ ‒

ἐπορθοβοάϲω·‎ 142 reiz||H

⏑‒ ‒ ⏑ ‒ ‒ ⏑ ⏑[^2]

†ἰαχὰν ἀοιδὰν μέλοϲ‎ 143a ?

‒⏑ ‒ ⏑ ‒ ‒

Ἀίδα, πάτερ, ϲοὶ†‎ 143b ith?

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

κατὰ γᾶϲ ἐνέπω γόουϲ‎ 144 T

‒ ⏑‒ ⏑ ⏑ ‒ ‒

οἷϲ ἀεὶ τὸ κατʼ ἦμαρ‎ 145 pher||Ba

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

λείβομαι, κατὰ μὲν φίλαν‎ 146 gly

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ὄνυχι τεμνομένα δέραν‎ 147 gly

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

χέρα τε κρᾶτʼ ἔπι κούριμον‎ 148 gly

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

τιθεμένα θανάτωι ϲῶι.‎ 149 pher

MESODE 2: 150-156

⏑ ⏑ ‒ ⏑ ⏑ ‒

‎ἒ ἔ, δρύπτε κάρα·‎ 150 hex

‒⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

οἷα δέ τιϲ κύκνοϲ ἀχέταϲ‎ 151 ibyc

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ποταμίοιϲ παρὰ χεύμαϲιν‎ 152 gly

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

πατέρα φίλτατον καλεῖ,‎ 153 lek||H

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

ὀλόμενον δολίοιϲ βρόχων‎ 154 gly

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑‒

ἕρκεϲιν, ὣϲ ϲὲ τὸν ἄθλιον,‎ 155 ibyc

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒⏑ ‒

πάτερ, ἐγὼ κατακλαίομαι,‎ 156 gly

ANTISTROPHE 2: 157-166

‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑

‎λουτρὰ πα|νύϲταθʼ ὑ|δρανάμε|νον χροῒ‎ 157 dac4

‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒

κοίται ἐ|ν οἰκτροτά|ται θανά|του.‎ 158 dac4\^

⏑‒ ⏑ ⏑‒ ‒

ἰώ μοι‎ 〈ἰώ〉‎ μοι‎ 159 reiz||Hs

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

πικρᾶϲ μὲν πελέκεωϲ τομᾶϲ‎ 160 gly

‒ ⏑ ‒ ⏑ ‒ ‒

ϲᾶϲ, πάτερ, πικρᾶϲ δ‎ʼ †ἐκ‎ 161a ith?

‒ ‒ ⏑ ‒ ‒ ‒

Τροίαϲ ὅδου βουλᾶϲ†‎. 161b ?

‒ ⏑ ‒ ⏑ ⏑ ‒ ∩

οὐ μίτραιϲι γυνά ϲε‎ 162 pher||B

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

δέξατʼ οὐδʼ ἐπὶ ϲτεφάνοιϲ,‎ 163 wil

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ξίφεϲι δʼ ἀμφιτόμοιϲ λυγρὰν‎ 164 gly

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

Αἰγίϲθου λώβαν θεμένα‎ 165 wil

⏑ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒

δόλιον ἔϲχεν ἀκοίταν.‎ 166 pher

**PARODOS: 167-214**

STROPHE: 167-189

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ |‒ ‒

ΧΟΡΟϹ‎ Ἀγαμέμνονοϲ ὦ κόρα, ἤλυθον, Ἠ|λέκτρα,‎ 167 A+sp

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ποτὶ ϲὰν ἀγρότειραν αὐλάν.‎ 168 diom||

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ | ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἔμολέ τιϲ ἔμο|λεν γαλακτοπόταϲ ἀνὴρ‎ 169 ia+gly

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

Μυκηναῖοϲ οὐριβάταϲ·‎ 170 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἀγγέλλει δʼ ὅτι νῦν τριταί-‎ 171 gly∫

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

αν καρύϲϲουϲιν θυϲίαν‎ 172 wil

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

Ἀργεῖοι, πᾶϲαι δὲ παρʼ Ἥ-‎ 173 wil∫

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ | ‒ ‒

ραν μέλλουϲιν παρθενικαὶ |ϲτείχειν.‎ 174 wil+sp

‒ ⏑ ‒ ⏑⏑‒ ⏑ ‒

Ηλ.‎ οὐκ ἐπʼ ἀγλαΐαιϲ, φίλαι,‎ 175 gly

‒ ⏑ ‒ ⏑ ⏑ ‒⏑ ‒

θυμὸν οὐδʼ ἐπὶ χρυϲέοιϲ‎ 176 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ὅρμοιϲ ἐκπεπόταμαι‎ 177 pher

⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

τάλαινʼ,‎ οὐδʼ ἱϲτᾶϲα χοροὺϲ‎ 178 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

Ἀργείαιϲ ἅμα νύμφαιϲ‎ 179 pher

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

εἱλικτὸν κρούϲω πόδʼ ἐμόν.‎ 180 wil

⏑ ⏑ ⏑ ⏑ ‒ |‒ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

δάκρυϲι νυχεύ|ω, δακρύων| δέ μοι μέλει‎ 181-2 ia+ “chor” +ia

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

δειλαίαι τὸ κατʼ ἦμαρ.‎ 183 pher||

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ϲκέψαι μου πιναρὰν κόμαν‎ 184 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

καὶ τρύχη τάδʼ ἐμῶν πέπλων,‎ 185 gly

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

εἰ πρέποντʼ Ἀγαμέμνονοϲ‎ 186 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

κούραι τᾶι βαϲιλείαι‎ 187 pher

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

τᾶι Τροίαι θʼ,‎ ἃ‎ ʼμοῦ πατέροϲ‎ 188 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ∩

μέμναταί ποθʼ ἁλοῦϲα.‎ 189 pher

ANTISTROPHE: 190-212

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ | ‒ ‒

Χο.‎ μεγάλα θεόϲ·‎ ἀλλʼ ἴθι καὶ παρʼ ἐμοῦ |χρῆϲαι‎ 190 A + sp

⏑ ⏑ ‒ ⏑ ‒ ⏑⏑ ‒ ‒

πολύπηνα φάρεα δῦναι‎ 191 “chor enop B”||

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ |‒ ‒ ‒ ⏑ ‒ ⏑⏑‒

χρύϲεά τε χάρι|ϲιν προϲθήματʼ ἀγλαΐαϲ.‎ 192 ia+wil

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

δοκεῖϲ τοῖϲι ϲοῖϲ δακρύοιϲ‎ 193 wil

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

μὴ τιμῶϲα θεοὺϲ κρατή-‎ 194 gly∫

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

ϲειν ἐχθρῶν;‎ οὔτοι ϲτοναχαῖϲ‎ 195 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἀλλʼ εὐχαῖϲι θεοὺϲ ϲεβί-‎ 196 gly∫

‒ ‒ ‒ ‒ ‒ ⏑ ⏑‒ | ‒ ‒

ζουϲʼ ἕξειϲ εὐαμερία|ν, ὦ παῖ.‎ 197 wil+sp

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑‒

Ηλ.‎ οὐδεὶϲ θεῶν ἐνοπᾶϲ κλύει‎ 198 gly

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

τᾶϲ δυϲδαίμονοϲ, οὐ παλαι-‎ 199 gly∫

‒ ⏑ ‒ ⏑⏑ ‒ ‒

ῶν πατρὸϲ ϲφαγιαϲμῶν.‎ 200 pher

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

οἴμοι τοῦ καταφθιμένου‎ 201 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

τοῦ τε ζῶντοϲ ἀλάτα,‎ 202 pher

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

ὅϲ που γᾶν ἄλλαν κατέχει‎ 203 wil

⏑ ⏑⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ | ⏑ ‒ ⏑‒

μέλεοϲ ἀλαί|νων ποτὶ θῆϲ|ϲαν ἑϲτίαν,‎ 204-5 ia + “chor” + ia

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

τοῦ κλεινοῦ πατρὸϲ ἐκφύϲ.‎ 206 pher||

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

αὐτὰ δʼ ἐν χερνῆϲι δόμοιϲ‎ 207 wil

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

ναίω ψυχὰν τακομένα‎ 208 wil

‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒

δωμάτων φυγὰϲ πατρίων‎ 209 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

οὐρείαϲ ἀνʼ ἐρίπναϲ.‎ 210 pher

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

μάτηρ δʼ ἐν λέκτροιϲ φονίοιϲ‎ 211 wil

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ἄλλωι ϲύγγαμοϲ οἰκεῖ.‎ 212 pher

213-214: ia3 (nonlyric)

**FIRST EPISODE: 215-431**

213-431: ia3: nonlyric

extrametric: 261a *pheu,* Orestes

extrametric: 281a *pheu*, Orestes

extrametric: 366a *pheu*, Orestes

[[]{#OLE_LINK4 .anchor}]{#OLE_LINK3 .anchor}Note one-line lacuna after
308

Diggle brackets 360

Diggle brackets 373-379

Diggle brackets 386-390

**FIRST STASIMON: 432-486**

*432-486: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 432-441

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒ |⏑ ‒ ‒

κλειναὶ νᾶεϲ, αἵ ποτʼ ἔβα|τε Τροίαν‎ 432 wil+ba

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

τοῖϲ ἀμετρήτοιϲ ἐρετμοῖϲ‎ 433 wil

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒ | ‒ ‒

πέμπουϲαι χορεύματα Νη|ρήιδων,‎ 434 wil+sp

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἵνʼ ὁ φίλαυλοϲ ἔπαλλε δελ-‎ 435 gly∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

φὶϲ πρώιραιϲ κυανεμβόλοι-‎ 436 gly∫

⏑ ‒ ‒ ⏑ ⏑ ‒

ϲιν εἱλιϲϲόμενοϲ,‎ 437 hex

⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

πορεύων τὸν τᾶϲ Θέτιδοϲ‎ 438 wil

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

κοῦφον ἅλμα ποδῶν Ἀχιλῆ‎ 439 sdd

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒⏑‒

ϲὺν Ἀγαμέμνονι Τρωίαϲ‎ 440 gly

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ἐπὶ Ϲιμουντίδαϲ ἀκτάϲ.‎ 441 pher

ANTISTROPHE 1: 442-451

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ | ⏑ ‒ ‒

Νηρῆιδεϲ δʼ Εὐβοῖδαϲ ἄκραϲ |λιποῦϲαι‎ 442 wil+ba

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

μόχθουϲ ἀϲπιϲτὰϲ ἀκμόνων‎ 443 wil

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ | ‒ ‒

Ἡφαίϲτου χρυϲέων ἔφερον| τευχέων, 444 wil+sp

‎ ⏑ ⏑ ⏑ ‒ ⏑⏑ ⏑ ⏑ ⏑ ‒

ἀνά τε Πήλιον ἀνά τʼ ἐρυ-‎ 445 gly∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

μνᾶϲ Ὄϲϲαϲ ἱερὰϲ νάπαϲ‎ 446 gly

‒ ‒ ‒ ⏑ ⏑ ‒

Νυμφαίαϲ ϲκοπιὰϲ‎ 447 hex

⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ‒

†κόραϲ μάτευϲ‎ʼ† ἔνθα πατὴρ‎ 448 ?[^3]

‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ἱππόταϲ τρέφεν Ἑλλάδι φῶϲ‎ 449 sdd

⏑ ⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ‒

Θέτιδοϲ εἰναλίαϲ γόνον‎ 450 gly

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ταχύπορον πόδʼ Ἀτρείδαιϲ.‎ 451 pher

STROPHE 2: 452-463

‒ ⏑⏑ |‒ ⏑ ⏑|‒ ⏑ ⏑ | ‒ ⏑ ⏑| ‒

‎Ἰλιό|θεν δʼ ἔκλυ|όν τινοϲ| ἐν λιμέ|ϲιν‎ 452 dac5\^

‒ ⏑‒ ⏑ ‒ ‒

Ναυπλίοιϲ βεβῶτοϲ‎ 453 ith||BaHa

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

τᾶϲ ϲᾶϲ, ὦ Θέτιδοϲ παῖ,‎ 454 pher||

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

κλεινᾶϲ ἀϲπίδοϲ ἐν κύκλωι‎ 455 gly

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

τοιάδε ϲήματα‎ †δείματα‎ 456 ?[^4]

⏑ ⏑⏑ ⏑ ‒ ‒

Φρύγια†‎ τετύχθαι·‎ 457 ?||Ha[^5]

⏑ ⏑ ⏑ ‒ ⏑ ⏑⏑⏑ ⏑ ‒

περιδρόμωι μὲν ἴτυοϲ ἕδραι‎ 458 gly

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ⏑ ‒

Περϲέα λαιμοτόμα|ν ὑπὲρ ἁλὸϲ‎ 459 D+cr

⏑ ‒ ‒| ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒

ποτανοῖ|ϲι πεδίλοιϲ| κορυφὰν Γορ|γόνοϲ ἴϲχειν,‎ 460-1 ba+ion3

⏑⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

Διὸϲ ἀγγέλωι ϲὺν Ἑρμᾶι,‎ 462 anacr

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

τῶι Μαίαϲ ἀγροτῆρι κούρωι.‎ 463 hipp

ANTISTROPHE 2: 464-475

‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑| ‒ ⏑⏑ | ‒

ἐν δὲ μέ|ϲωι κατέ|λαμπε ϲά|κει φαέ|θων‎ 464 dac5\^

‒ ⏑ ‒ ⏑‒∩

κύκλοϲ ἁλίοιο‎ 465 ith||BH

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ἵπποιϲ ἂμ πτεροέϲϲαιϲ‎ 466 pher||

‒ ‒ ‒ ⏑⏑‒ ⏑ ‒

ἄϲτρων τʼ αἰθέριοι χοροί,‎ 467 gly

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

Πλειάδεϲ Ὑάδεϲ,‎ †Ἕκτοροϲ‎ 468 ?[^6]

‒ ⏑ ⏑ ⏑ ‒ ‒

ὄμμαϲι†‎ τροπαῖοι·‎ 469 ?||H[^7]

⏑ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

ἐπὶ δὲ χρυϲοτύπωι κράνει‎ 470 gly

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ |⏑ ⏑ ⏑ ‒

Ϲφίγγεϲ ὄνυξιν ἀοί|διμον ἄγραν‎ 471 D+cr

⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ | ⏑⏑ ‒ ‒

φέρουϲαι·|‎ περιπλεύρωι |δὲ κύτει πύρ|πνοοϲ ἔϲπευ-‎ 472-3 ba+ion3∫

⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

δε δρόμωι λέαινα χαλαῖϲ‎ 474 anacr

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ∩

Πειρηναῖον ὁρῶϲα πῶλον.‎ 475 hipp

EPODE: 475-6-486

[]{#_Hlk47357342 .anchor}‒ ⏑ ⏑ | ‒ ⏑ ⏑ |‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ⏑ ⏑ | ‒ ‒

‎ἄορι |δʼ ἐν φονί|ωι τετρα|βάμονε|ϲ ἵπποι ἔ|παλλον,‎ 475-6 dac6

⏑ ‒ ‒ [[]{#OLE_LINK2 .anchor}]{#OLE_LINK1 .anchor}| ‒ ⏑ ‒ ⏑⏑ ⏑ ⏑ ‒

κελαινὰ |δʼ ἀμφὶ νῶθʼ ἵετο κόνιϲ.‎ 477-8 ba+lek||

‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ⏑ ‒

τοιῶνδʼ ἄνακ|τα δοριπόνων‎ 479 ia2

⏑ ⏑ ⏑ ‒ ‒ ‒ ⏑ ∩

ἔκανεν ἀνδρῶν, Τυνδαρί,‎ 480 lek||B

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ | ‒ ⏑ ‒

ϲὰ λέχεα, κακό|φρον κόρα.‎ 481 ia+cr

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

τοιγάρ ϲοί ποτʼ οὐρανίδαι‎ 483[^8] wil[^9]

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

πέμψουϲιν θανάτου δίκαν.‎ 484 gly

⏑ ⏑ ⏑ ⏑ ⏑⏑| ⏑ ⏑ ⏑ ‒

ἔτʼ ἔτι φόνιο|ν ὑπὸ δέραν‎ 485 ia+cr

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ὄψομαι αἷμα χυθὲν ϲιδάρωι. 486 decasyll

**SECOND EPISODE: 487-698**

487-584: ia3: nonlyric

extrametric: 557a *ea,* Orestes

Note one-line lacuna after 538

*585-595: lyrics (lyric, astrophic, choral song)*

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ⏑⏑ ‒ ⏑ ‒

ἔμολεϲ ἔμολεϲ, ὤ,| χρόνιοϲ ἁμέρα,‎ 585 doc2

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

κατέλαμψαϲ, ἔδειξαϲ ἐμφανῆ‎ 586 cyren

⏑ ‒ ‒ ⏑ ‒ | ⏑ ‒ ‒ ⏑ ‒

πόλει πυρϲόν, ὃϲ |παλαιᾶι φυγᾶι‎ 587 doc2

⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒

πατρίων ἀπὸ δωμάτων τάλαϲ‎ 588 cyren

⏑ ‒ ‒ ⏑ ‒

ἀλαίνων ἔβα.‎ 589 doc

⏑⏑ ‒ ⏑⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒

θεὸϲ αὖ θεὸϲ ἁμετέραν τιϲ ἄγει‎ 590 A

‒ ‒ ‒ ⏑ ‒

νίκαν, ὦ φίλα.‎ 591 doc

⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑ | ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἄνεχε χέραϲ, ἄνεχε |λόγον, ἵει λιτὰϲ‎ 592-3 doc2

‒ ⏑ ‒ | ⏑ ‒ ‒ ⏑ ‒

ἐϲ θεούϲ,| τύχαι ϲοι τύχαι‎ 594 cr+doc

⏑ ‒ ‒ ⏑ ‒ | ⏑ ‒ ‒ ⏑ ‒

καϲίγνητον ἐμ|βατεῦϲαι πόλιν.‎ 595 doc2

596-698: ia3 (nonlyric)

Diggle adds a one-line lacuna, including a change of speaker to the Old
Man, after 651.

Diggle brackets 685-689

**SECOND STASIMON: 699-746**

*699-746: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 699-712

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒

ἀταλᾶϲ ὑπὸ‎ †ματέροϲ Ἀργείων†‎ 699 ?[^10]

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ὀρέων ποτὲ κληδὼν‎ 700 reiz

‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ‒ ‒

ἐν πολιαῖϲι μένει φήμαιϲ‎ 701 ibyc/chol

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

εὐαρμόϲτοιϲ ἐν καλάμοιϲ‎ 702 wil

‒ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒

Πᾶνα μοῦϲαν ἡδύθροον‎ 703 wil

[]{#_Hlk47361138 .anchor}⏑‒ ‒ ‒ ⏑ ⏑ ‒

πνέοντʼ,‎ ἀγρῶν ταμίαν,‎ 704 hept

‒ ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

χρυϲέαν ἄρνα καλλίποκον‎ 705 wil

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

πορεῦϲαι.‎ πετρίνοιϲ δʼ ἐπι-‎ 706 gly∫

‒ ‒ ‒ ⏑⏑ ‒ ⏑ ‒

ϲτὰϲ κᾶρυξ ἰαχεῖ βάθροιϲ·‎ 707 gly

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ἀγορὰν ἀγοράν, Μυκη-‎ 708 T∫

‒ ‒ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

ναῖοι, ϲτείχετε μακαρίων‎ 709 gly

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ὀψόμενοι τυράννων‎ 710 arist

‒ ⏑ ⏑ ‒ ⏑ ⏑

φάϲματα‎ †δείματα.‎ 711 ?

⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

χοροὶ δ‎ʼ† Ἀτρειδᾶν ἐγέραιρον οἴκουϲ.‎ 712 ?

ANTISTROPHE 1: 713-726

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ | ‒ ⏑ ‒

‎θυμέλαι δʼ ἐπίτναντο χρυ|ϲήλατοι,‎ 713 gly+cr?

⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

ϲελαγεῖτο δʼ ἀνʼ ἄϲτυ‎ 714 reiz

‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒ ‒

πῦρ ἐπιβώμιον Ἀργείων·‎ 715 ibyc/chol

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

λωτὸϲ δὲ φθόγγον κελάδει‎ 716 wil

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒

κάλλιϲτον, Μουϲᾶν θεράπων,‎ 717 wil

‒ ‒ ‒ ‒ ⏑ ⏑ ‒

μολπαὶ δʼ ηὔξοντʼ ἐραταὶ‎ 718 hept

‒ ⏑ ‒ ‒ ⏑ ⏑ ⏑ ⏑ ‒

χρυϲέαϲ ἀρνὸϲ‎ †ἐπίλογοι†‎ 719 ?

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

Θυέϲτου·‎ κρυφίαιϲ γὰρ εὐ-‎ 720 gly∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ναῖϲ πείϲαϲ ἄλοχον φίλαν‎ 721 gly

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

Ἀτρέωϲ, τέραϲ ἐκκομί-‎ 722 T∫

‒ ‒ ‒ ⏑ ⏑ ⏑⏑ ⏑ ‒

ζει πρὸϲ δώματα·‎ νεόμενοϲ δʼ‎ 723 gly

‒ ⏑ ⏑ ‒ ⏑‒ ‒

εἰϲ ἀγόρουϲ ἀυτεῖ‎ 724 arist

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

τὰν κερόεϲϲαν ἔχειν‎ 725 D

‒ ⏑⏑ ‒ | ‒ ⏑ ⏑ ‒ | ⏑ ‒ ‒

χρυϲεόμαλ|λον κατὰ δῶ|μα ποίμναν.‎ 726 chor2+ba

STROPHE 2: 727-736

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

τότε δὴ τότε‎ 〈δὴ〉‎ φαεν-‎ 727 T∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

νὰϲ ἄϲτρων μετέβαϲʼ ὁδοὺϲ‎ 728 gly

‒ ‒ ‒ ⏑ ‒ ⏑⏑ ‒

Ζεὺϲ καὶ φέγγοϲ ἀελίου‎ 729 wil

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

λευκόν τε πρόϲωπον ἀοῦϲ,‎ 730 hag

⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

τὰ δʼ ἕϲπερα νῶτʼ ἐλαύνει‎ 731 hag

‒ ‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

θερμᾶι φλογὶ θεοπύρωι,‎ 732 tel

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

νεφέλαι δʼ ἔνυδροι πρὸϲ ἄρκτον,‎ 733 diom

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒

ξηραί τʼ Ἀμμωνίδεϲ ἕδραι‎ 734 oct||

‒ ‒ ⏑ ‒ ⏑ ⏑ ‒

φθίνουϲʼ ἀπειρόδροϲοι,‎ 735 hept

‒ ‒ ‒ ‒ ‒ ⏑⏑ ‒ | ⏑ ‒ ‒

καλλίϲτων ὄμβρων Διόθεν| ϲτερεῖϲαι.‎ 736 wil+ba

ANTISTROPHE 2: 737-746

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒

‎λέγεται‎ 〈τάδε〉‎, τὰν δὲ πί-‎ 737 T∫

‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

ϲτιν ϲμικρὰν παρʼ ἔμοιγʼ ἔχει,‎ 738 gly

‒ ‒ ‒ ‒ ‒ ⏑⏑‒

ϲτρέψαι θερμὰν ἀέλιον‎ 739 wil

‒ ‒ ⏑ ⏑ ‒ ‒ ‒ ‒

χρυϲωπὸν ἕδραν ἀλλάξαν-‎ 740 hag/chol∫[^11]

⏑ ‒ ⏑ ⏑‒ ⏑ ‒ ‒

τα δυϲτυχίαι βροτείωι‎ 741 hag

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒

θνατᾶϲ ἕνεκεν δίκαϲ.‎ 742 tel

⏑ ⏑ ‒ ⏑ ⏑ ‒⏑ ‒ ‒

φοβεροὶ δὲ βροτοῖϲι μῦθοι‎ 743 diom

‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ‒

κέρδοϲ πρὸϲ θεῶν θεραπείαν.‎ 744 oct||

‒ ‒ ‒ ‒ ⏑ ⏑ ‒

ὧν οὐ μναϲθεῖϲα πόϲιν‎ 745 hept

‒ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ | ⏑ ‒ ‒

κτείνειϲ, κλεινῶν ϲυγγενέτει|ρʼ ἀδελφῶν.‎ 746 wil+ba

**THIRD EPISODE: 746a**[^12]**-1146**

extrametric: 746a *ea ea*, chorus

747-859: ia3: nonlyric

*860-865: lyrics (lyric, strophe)*

STROPHE: 860-865

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

θὲϲ ἐϲ χορόν, ὦ φίλα, ἴχνοϲ, ὡϲ νεβρὸϲ οὐράνιον‎ 860 ⏑D⏑D

‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑⏑‒

πήδημα κουφίζουϲα ϲὺν ἀγλαΐαι.‎ 861 ‒e‒D

‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

νικᾶι ϲτεφαναφόρα κρείϲϲω τῶν παρʼ Ἀλφειοῦ‎ ῥεέθροιϲ τελέϲαϲ 862-3
‒D‒e‒D

⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑

καϲίγνη|τοϲ ϲέθεν·‎ ἀλλʼ ὑπάειδε‎ 864 ba+D⏑

‒ ⏑ ‒ ⏑ ‒ ‒ | ⏑ ‒ ⏑ ‒

καλλίνικον ὠιδὰ|ν ἐμῶι χορῶι.‎ 865 ith+ia

866-873: ia3: nonlyric

*874-879: lyrics (lyric, antistrophe)*

ANTISTROPHE: 874-879

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ϲὺ μέν νυν ἀγάλματʼ ἄειρε κρατί·‎ τὸ δʼ ἁμέτερον‎ 874 ⏑D⏑D

‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

χωρήϲεται Μούϲαιϲι χόρευμα φίλον.‎ 875 ‒e‒D

‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

νῦν οἱ πάροϲ ἁμετέραϲ γαίαϲ τυραννεύϲουϲι φίλοι βαϲιλῆϲ‎ 876-7 ‒D‒e‒D

⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑

δικαίωϲ|, τοὺϲ ἀδίκουϲ καθελόντεϲ.‎ 878 ba+D⏑

‒ ⏑ ‒ ⏑ ‒ ‒ | ⏑‒ ⏑ ‒

ἀλλʼ ἴτω ξύναυλοϲ |βοὰ χαρᾶι.‎ 879 ith+ia

880-987: ia3 (nonlyric)

extrametric: 968a *pheu*, Orestes

extrametric: 987a io, chorus

Diggle adds a lacuna of indefinite length, including a change of speaker
to Orestes, after 965.

*987a-997: anapests (nonlyric)*

‒ ‒

ἰώ,‎ 987a e.m.

⏑ ⏑ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ‒ ‒ ‒

βαϲίλεια γύναι χ|θονὸϲ Ἀργείαϲ,‎ 988 an2

‒ ‒ ⏑ ⏑‒

παῖ Τυνδάρεω,‎ 989 an

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

καὶ τοῖν ἀγαθοῖν| ξύγγονε κούροιν‎ 990 an2

⏑⏑ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

Διόϲ, οἳ φλογερὰ|ν αἰθέρʼ ἐν ἄϲτροιϲ‎ 991 an2

‒ ‒ ⏑ ⏑ ‒ |⏑ ⏑ ‒ ⏑ ⏑ ‒

ναίουϲι, βροτῶ|ν ἐν ἁλὸϲ ῥοθίοιϲ‎ 992 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒

τιμὰϲ ϲωτῆ|ραϲ ἔχοντεϲ·‎ 993 par

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

χαῖρε, ϲεβίζω |ϲʼ ἴϲα καὶ μάκαραϲ‎ 994 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

πλούτου μεγάληϲ |τʼ εὐδαιμονίαϲ.‎ 995 an2

‒ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ‒ ‒ ‒

τὰϲ ϲὰϲ δὲ τύχαϲ |θεραπεύεϲθαι‎ 996 an2

‒ ⏑ ‒ ⏑ ⏑ ‒ ‒

†καιρόϲ, ὦ βαϲίλεια†‎. 997 ?

998-1146: ia3: nonlyric

Diggle adds a one-line lacuna after 1045.

Diggle brackets 1097-1101.

Diggle places line 1107-1108 between 1131 and 1132.

**THIRD STASIMON: 1147-1171**

*1147-1164: lyrics (lyric, includes responding stanzas)*

STROPHE: 1147-1154+lacuna

⏑ ‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ‒ ⏑ ‒

ἀμοιβαὶ κακῶν|·‎ μετάτροποι πνέου-‎ 1147 doc2∫

⏑ ‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ‒ ‒ ‒

ϲιν αὖραι δόμων.‎| τότε μὲν‎ 〈ἐν〉‎ λουτροῖϲ‎ 1148 doc2

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ | ⏑ ‒ ⏑ ‒

ἔπεϲεν ἐμὸϲ ἐ|μὸϲ ἀρχέταϲ,‎ 1149 ia2

⏑‒ ‒ ⏑ ‒ | ⏑ ‒ ‒⏑‒

ἰάχηϲε δὲ ϲ|τέγα λάινοί‎ 1150 doc2

⏑ ‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ‒ ⏑ ‒

τε θριγκοὶ δόμων,| τάδʼ ἐνέποντοϲ·‎ Ὦ‎ 1151 doc2

⏑ ⏑⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ‒ ⏑ ‒

ϲχέτλιε, τί με, γύναι, φ|ονεύϲειϲ φίλαν‎ 1152 doc2

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

πατρίδα δεκέτεϲι‎ 1153 doc

⏑ ‒ ⏑ ‒ | ‒ ⏑ ‒

ϲποραῖϲιν ἐλ|θόντʼ ἐμάν;‎ 1154 ia+cr||Ba

〈

〉.[^13]

ANTISTROPHE: 1155-64

⏑ ‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ‒ ⏑ ‒

‎παλίρρουϲ δὲ τάν|δʼ ὑπάγεται δίκα‎ 1155 doc2

⏑⏑ ⏑ ‒ ⏑ ‒ | ⏑ ⏑⏑ ‒ ⏑ ‒

διαδρόμου λέχουϲ,| μέλεον ἃ πόϲιν‎ 1156 doc2

⏑ ⏑⏑ ⏑ ⏑ ⏑ | ⏑ ‒ ‒ ‒

χρόνιον ἱκόμε|νον εἰϲ οἴκουϲ‎ 1157 ia2[^14]

⏑ ‒ ‒ ⏑ ‒ |⏑ ⏑ ‒ ‒ ⏑ ‒

Κυκλώπειά τʼ οὐ|ράνια τείχεʼ ὀ-‎ 1158 doc2∫

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒

ξυθήκτωι‎ †βέλουϲ ἔκανεν†‎ αὐτόχειρ,‎ 1159 ?

⏑ ⏑ ⏑ ‒ ⏑ ‒ | ⏑ ‒ ‒ ‒ ‒

πέλεκυν ἐν χεροῖν| λαβοῦϲʼ.‎ ὦ τλάμων‎ 1160 doc2

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

πόϲιϲ, ὅτι ποτὲ τὰν‎ 1161 doc

⏑ ‒ ⏑ ‒ | ‒ ⏑ ∩

τάλαιναν ἔϲ|χεν κακόν.‎ 1162 ia+cr||B

⏑ ‒ ‒ ⏑ ‒ | ⏑ ‒ ‒ ⏑ ‒

ὀρεία τιϲ ὡϲ| λέαινʼ ὀργάδων‎ 1163 doc2

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒| ⏑ ⏑ ⏑ ‒ ⏑ ∩

δρύοχα νεμομένα| τάδε κατήνυϲεν. 1164 doc2

1165: ia3 (nonlyric, cry from indoors)

Κλ.‎ (ἔϲωθεν‎)

‒ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ∩

ὦ τέκνα, πρὸϲ | θεῶν, μὴ κτάνη|τε μητέρα.‎ 1165 ia3[^15]

*1166-1171: lyrics (lyric, astrophic)*; ia3 (nonlyric?) 1168

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

Χο.‎ κλύειϲ ὑπώ|ροφον βοάν;‎ 1166 ia2

⏑‒ ‒ ‒

Κλ.‎ ἰώ μοί μοι.‎ 1167 doc

‒ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒

Χο.‎ ὤιμωξα κἀ|γὼ πρὸϲ τέκνων |χειρουμένηϲ. ‎ 1168 ia3

⏑ ‒ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ‒ ⏑ ‒

νέμει τοι δίκαν |θεόϲ, ὅταν τύχηι.‎ 1169 doc2

⏑ ⏑⏑ ⏑ ⏑ ⏑ ⏑ ⏑| ⏑ ⏑⏑ ‒ ⏑ ‒

ϲχέτλια μὲν ἔπαθεϲ, ἀ|νόϲια δʼ εἰργάϲω,‎ 1170 doc2

⏑ ‒ ‒ ⏑ ‒

τάλαινʼ,‎ εὐνέταν.‎ 1171 doc

**EXODOS: 1172-1359 (=5^th^ Episode)**

1172-1176: ia3 (nonlyric)

‒ ‒ ⏑ ‒ | ‒ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

ἀλλʼ οἵδε μη|τρὸϲ νεοφόνοι|ϲ ἐν αἵμαϲιν‎ 1172 ia3

⏑ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒ | ‒ ‒ ⏑ ∩

πεφυρμένοι| βαίνουϲιν ἐ|ξ οἴκων πόδα‎ 1173 ia3

〈 〉

⏑ ‒⏑ ‒ | ⏑ ‒ ⏑‒ | ‒ ‒ ⏑ ‒

τροπαῖα, δείγ|ματʼ ἀθλίων| προϲφαγμάτων.‎ 1174 ia3

‒ ‒ ⏑ ‒| ‒ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

οὐκ ἔϲτιν οὐ|δεὶϲ οἶκοϲ ἀθ|λιώτεροϲ‎ 1175 ia3

‒ ‒ ⏑ ‒| ‒ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

τῶν Τανταλεί|ων οὐδʼ ἔφυ| ποτʼ ἐκγόνων.‎ 1176 ia3

*1177-1232: lyrics (lyric, includes responding stanzas, kommos)*; ia3
(lyric?) 1182-1183=1198-1199, 1206=1213-1214, 1209-1217, 1221=1227

STROPHE 1: 1177-1189

⏑‒ ‒ | ‒ ‒ ‒ | ‒ ⏑ ‒

Ορ.‎ ἰὼ Γᾶ |καὶ Ζεῦ παν|δερκέτα‎ 1177 ba+mol+cr

⏑ ‒ ⏑ ⏑⏑ | ⏑ ‒ ⏑ ⏑ ⏑

βροτῶν, ἴδετε| τάδʼ ἔργα φόνι-‎ 1178 ia2∫

⏑ ⏑ ⏑ ⏑ ⏑ ⏑| ⏑ ‒ ⏑ ‒

α μυϲαρά, δίγο|να ϲώματʼ ἐν‎ 1179 ia2

[]{#_Hlk47556356 .anchor} ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

†χθονὶ κείμενα πλαγᾶι†‎ 1180 ?[^16]

⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

χερὸϲ ὕπʼ ἐμᾶ|ϲ, ἄποινʼ ἐμῶν‎ 1181a ia2

‒ ⏑ ‒

πημάτων‎〈 1181b ?

〉. ?

⏑ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒| ⏑‒ ⏑ ‒

Ηλ.‎ δακρύτʼ ἄγα|ν, ὦ ϲύγγονʼ,‎ αἰ|τία δʼ ἐγώ.‎ 1182 ia3

⏑⏑ ⏑ ⏑ ⏑ ⏑ | ⏑ ‒ ⏑ ‒ |⏑ ‒ ⏑ ‒

διὰ πυρὸϲ ἔμο|λον ἁ τάλαι|να ματρὶ τᾶιδʼ,‎ 1183 ia3

‒ ⏑‒ ⏑ ‒ ‒

ἅ μʼ ἔτικτε κούραν.‎ 1184 ith

⏑‒ ⏑ ‒ | ‒ ⏑ ‒

〈Χο.〉‎ ἰὼ τύχαϲ‎ |†ϲᾶϲ τύχαϲ‎ 1185 ia+cr

‒ ‒ ⏑ ‒ | &lt;⏑ ‒ ‒&gt;

μᾶτερ τεκοῦϲ‎ʼ† 1186 ia+ba

⏑ ‒ ⏑ ⏑ ⏑ |⏑ ‒ ⏑ ‒

ἄλαϲτα μέλε|α καὶ πέρα‎ 1187 ia2

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

παθοῦϲα ϲῶν| τέκνων ὑπαί.‎ 1188 ia2

⏑ ‒ ⏑ ‒| ‒ ⏑ ‒ ⏑ ‒ ‒

πατρὸϲ δʼ ἔτει|ϲαϲ φόνον δικαίωϲ.‎ 1189 ia+ith

ANTISTROPHE 1: 1190-1205

⏑‒ ‒ | ⏑ ‒ ‒ | ‒ ⏑ ‒

‎Ορ.‎ ἰὼ Φοῖ|βʼ,‎ ἀνύμνη|ϲαϲ δίκαιʼ‎ 1190 ba2+cr

⏑ ‒ ⏑ ⏑ ⏑ | ⏑ ‒ ⏑ ‒

ἄφαντα, φανε|ρὰ δʼ ἐξέπρα-‎ 1191 ia2∫

⏑ ⏑ ⏑ ⏑ ⏑ ⏑| ⏑ ‒ ⏑ ‒

ξαϲ ἄχεα, φόνι|α δʼ ὤπαϲαϲ‎ 1192 ia2

⏑ ⏑ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒

λάχεʼ ἀπὸ γᾶ|ϲ Ἑλλανίδοϲ.‎ 1193 ia+ “chor”

⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

τίνα δʼ ἑτέραν| μόλω πόλιν;‎ 1194 ia2

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

τίϲ ξένοϲ, τίϲ εὐϲεβὴϲ‎ 1195 lek

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

ἐμὸν κάρα |προϲόψεται‎ 1196 ia2

‒ ⏑ ‒ ⏑ ‒ ∩

ματέρα κτανόντοϲ;‎ 1197 ith

⏑‒⏑‒ | ‒ ‒ ⏑‒ | ⏑ ‒ ⏑ ‒

Ηλ.‎ ἰὼ ἰώ |μοι.‎ ποῖ δʼ ἐγώ,| τίνʼ ἐϲ χορόν,‎ 1198 ia3

⏑ ⏑ ⏑ ⏑ ‒ |⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

τίνα γάμον εἶ|μι;‎ τίϲ πόϲιϲ| με δέξεται‎ 1199 ia3

‒ ⏑ ‒ ⏑ ‒ ‒

νυμφικὰϲ ἐϲ εὐνάϲ;‎ 1200 ith

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

Χο.‎ πάλιν πάλιν |φρόνημα ϲὸν‎ 1201 ia2

⏑ ‒ ⏑ ‒ | ⏑ ‒ ‒

μετεϲτάθη |πρὸϲ αὔραν·‎ 1202 ia+ba

⏑ ‒ ⏑ ⏑ ⏑| ⏑ ‒ ⏑ ‒

φρονεῖϲ γὰρ ὅϲι|α νῦν, τότʼ οὐ‎ 1203 ia2

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

φρονοῦϲα, δει|νὰ δʼ εἰργάϲω,‎ 1204 ia2

⏑ ‒ ⏑ ‒| ‒ ⏑ ‒ ⏑ ‒ ∩

φίλα, καϲίγ|νητον οὐ θέλοντα.‎ 1205 ia+ith

STROPHE 2: 1206-1212

⏑ ‒ ⏑ ‒|⏑ ‒ ⏑ ‒ | ‒ ‒ ⏑ ‒

‎〈Ορ.〉‎ κατεῖδεϲ οἷ|ον ἁ τάλαι|νʼ ἔξω πέπλων‎ 1206 ia3

⏑ ⏑ ⏑ ⏑ ‒| ⏑ ‒ ⏑ ‒ | ⏑ ‒ ∩

ἔβαλεν ἔδει|ξε μαϲτὸν ἐν| φοναῖϲιν,‎ 1207 ia2+ba||B

⏑‒ ‒ | ‒ ⏑ ‒

ἰώ μοι, |πρὸϲ πέδωι‎ 1208 ba+cr

⏑ ‒ ⏑ ⏑ ⏑| ⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

τιθεῖϲα γόνι|μα μέλεα;‎ τα|κόμαν δʼ ἐγώ.‎ 1209 ia3

⏑ ‒ ⏑ ⏑ ⏑ | ⏑ ‒ ⏑ ‒

Χο.‎ ϲάφʼ οἶδα·‎ διʼ ὀ|δύναϲ ἔβαϲ,‎ 1210 ia2

⏑‒⏑‒ | ⏑ ‒ ⏑ ‒

ἰήιον |κλύων γόον‎ 1211 ia2

‒ ⏑ ‒ ⏑ ‒ ‒

ματρὸϲ ἅ ϲʼ ἔτικτεν.‎ 1212 ith

ANTISTROPHE 2: 1213-1220

⏑‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

‎Ορ.‎ βοὰν δʼ ἔλαϲ|κε τάνδε, πρὸϲ| γένυν ἐμὰν‎ 1213-14 ia3

⏑ ‒ ⏑ ‒ |⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ‒

τιθεῖϲα χεῖ|ρα·‎ Τέκοϲ ἐμόν,| λιταίνω.‎ 1215 ia2+ba||Bs

⏑ ‒ ‒ | ‒ ⏑ ‒

παρήιδων| τʼ ἐξ ἐμᾶν‎ 1216 ba+cr

⏑ ‒ ⏑ ‒ | ⏑ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

ἐκρίμναθʼ,‎ ὥϲ|τε χέραϲ ἐμὰϲ| λιπεῖν βέλοϲ.‎ 1217 ia3

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

Χο.‎ τάλαινα.‎ πῶϲ‎ |〈δ‎ʼ〉 ἔτλαϲ φόνον‎ 1218 ia2

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

διʼ ὀμμάτω|ν ἰδεῖν ϲέθεν‎ 1219 ia2

‒ ⏑ ‒ ⏑ ‒ ‒

ματρὸϲ ἐκπνεούϲαϲ;‎ 1220 ith

STROPHE 3: 1221-1226

⏑ ‒ ⏑ ⏑ ⏑| ⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

‎Ορ.‎ ἐγὼ μὲν ἐπι|βαλὼν φάρη |κόραιϲ ἐμαῖϲ‎ 1221 ia3

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

φαϲγάνωι κατηρξάμαν‎ 1222 lek

‒ ⏑ ⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

ματέροϲ ἔϲω |δέραϲ μεθείϲ.‎ 1223 ia2

⏑ ‒ ⏑ ⏑ ⏑ | ⏑ ‒ ⏑ ‒

Ηλ.‎ ἐγὼ δέ‎ 〈γ‎ʼ〉 ἐπε|κέλευϲά ϲοι‎ 1224 ia2

⏑ ‒ ⏑ ‒ | ⏑ ‒ ⏑ ‒

ξίφουϲ τʼ ἐφη|ψάμαν ἅμα.‎ 1225 ia2

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑

δεινότατον παθέων ἔρεξα.‎ 1226 decasyll

ANTISTROPHE 3: 1227-1232

⏑ ‒ ⏑ ‒ |⏑ ⏑ ⏑ ⏑ ‒ |⏑ ‒ ⏑ ‒

‎〈Ορ.〉‎ λαβοῦ, κάλυπ|τε μέλεα μα|τέροϲ πέπλοιϲ‎ 1227 ia3

‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

〈καὶ〉‎ καθάρμοϲον ϲφαγάϲ.‎ 1228 lek

⏑ ⏑⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

φονέαϲ ἔτικ|τεϲ ἆρά ϲοι.‎ 1229 ia2

⏑ ‒ ⏑ ‒ |⏑ ‒ ⏑ ‒

Ηλ.‎ ἰδού, φίλαι |τε κοὐ φίλαι‎ 1230 ia2

⏑ ⏑⏑ ⏑ ‒ | ⏑ ‒ ⏑ ‒

φάρεα τάδʼ ἀμ|φιβάλλομεν,‎ 1231 ia2

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

τέρμα κακῶν μεγάλων δόμοιϲιν.‎ 1232 decasyll

*1233-1237: anapests (nonlyric)*

‒ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

Χο.‎ ἀλλʼ οἵδε δόμω|ν ὕπερ ἀκροτάτων‎ 1233 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

βαίνουϲί τινεϲ| δαίμονεϲ ἢ θεῶν‎ 1234 an2

[]{#_Hlk47617162 .anchor} ‒ ‒ ⏑ ⏑‒ | ‒ ‒ ‒ ‒

τῶν οὐρανίω|‎ν· οὐ γὰρ θνητῶν γʼ‎ 1235 an2

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

ἥδε κέλευθοϲ.|‎ τί ποτʼ ἐϲ φανερὰν‎ 1236 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ∩

ὄψιν βαίνου|ϲι βροτοῖϲιν;‎ 1237 par||B

1238-1291: ia3 (nonlyric)

*1292-1359: anapests (nonlyric)*

‒ ‒ ⏑ ⏑‒ | ⏑ ⏑ ‒ ‒ ‒

Χο.‎ ὦ παῖδε Διόϲ,| θέμιϲ ἐϲ φθογγὰϲ‎ 1292 an2

‒ ‒ ⏑⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

τὰϲ ὑμετέρα|ϲ ἡμῖν πελάθειν;‎ 1293 an2

⏑ ⏑ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑⏑‒

Κα.‎ θέμιϲ, οὐ μυϲαραῖϲ| τοῖϲδε ϲφαγίοιϲ.‎ 1294 an2

‒ ‒ ⏑ ⏑‒ |‒ ⏑ ⏑ ‒ ‒

Χο.‎ πῶϲ ὄντε θεὼ |τῆϲδέ τʼ ἀδελφὼ‎ 1298 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

τῆϲ καπφθιμένη|ϲ οὐκ ἠρκέϲατον‎ 1299 an2

‒ ‒ ⏑ ⏑ ‒

Κῆραϲ μελάθροιϲ;‎ 1300 an

‒ ⏑ ⏑ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

Κα.‎ μοῖρά τʼ ἀνάγκη |τʼ ἦγʼ ἐϲ τὸ χρεὼν‎ 1301 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

Φοίβου τʼ ἄϲοφοι |γλώϲϲηϲ ἐνοπαί.‎ 1302 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

Ηλ.‎ κἀμοὶ μύθου |μέτα, Τυνδαρίδαι;‎ 1295 an2

‒ ‒ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

Κα.‎ καὶ ϲοί·‎ Φοίβωι |τήνδʼ ἀναθήϲω‎ 1296 an2

‒ ‒ ⏑ ⏑ ‒

πρᾶξιν φονίαν.‎ 1297 an

‒ ⏑ ⏑ ‒ ‒ | ‒ ‒ ‒ ‒

〈Ηλ.〉‎ τίϲ δʼ ἔμʼ Ἀπόλλων|, ποῖοι χρηϲμοὶ‎ 1303 an2

⏑ ⏑‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

φονίαν ἔδοϲαν| μητρὶ γενέϲθαι;‎ 1304 an2

[[]{#_Hlk47617496 .anchor}]{#_Hlk47617480 .anchor} ‒ ‒ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

Κα.‎ κοιναὶ πράξειϲ, |κοινοὶ δὲ πότμοι,‎ 1305 par

⏑ ⏑ ‒ ⏑ ⏑ ‒

μία δʼ ἀμφοτέρουϲ‎ 1306 an

‒ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ‒ ∩

ἄτη πατέρων |διέκναιϲεν.‎ 1307 par||B

‒ ‒ ⏑ ⏑ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

Ορ.‎ ὦ ϲύγγονέ μοι,| χρονίαν ϲʼ ἐϲιδὼν‎ 1308 an2

‒ ‒ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

τῶν ϲῶν εὐθὺϲ |φίλτρων ϲτέρομαι‎ 1309 an2

‒ ⏑ ⏑ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

καὶ ϲʼ ἀπολείψω |ϲοῦ λειπόμενοϲ.‎ 1310 an2

⏑ ⏑ ‒ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

Κα.‎ πόϲιϲ ἔϲτʼ αὐτῆι| καὶ δόμοϲ·‎ οὐχ ἥδʼ‎ 1311 an2

‒ ⏑ ⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

οἰκτρὰ πέπονθεν,| πλὴν ὅτι λείπει‎ 1312 an2

⏑ ⏑ ‒ ‒ ‒

πόλιν Ἀργείων.‎ 1313 an

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

Ηλ.‎ καὶ τίνεϲ ἄλλαι |ϲτοναχαὶ μείζουϲ‎ 1314 an2

‒ ‒ ⏑ ⏑ ‒ |⏑ ⏑ ‒ ‒ ‒

ἢ γῆϲ πατρία|ϲ ὅρον ἐκλείπειν;‎ 1315 an2

‒ ⏑ ⏑ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

〈Ορ.〉‎ ἀλλʼ ἐγὼ οἴκω|ν ἔξειμι πατρὸϲ‎ 1316 an2

⏑ ⏑ ‒ ⏑ ⏑‒ | ‒ ‒ ⏑ ⏑ ‒

καὶ ἐπʼ ἀλλοτρίαιϲ| ψήφοιϲι φόνον‎ 1317 an2

‒ ⏑ ⏑ ‒ ‒

μητρὸϲ ὑφέξω.‎ 1318 an

⏑ ⏑‒ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

Κα.‎ ὁϲίαν, θάρϲει,| Παλλάδοϲ ἥξειϲ‎ 1319 an2

⏑ ⏑ ‒ ⏑ ⏑ ‒

πόλιν·‎ ἀλλʼ ἀνέχου.‎ 1320 an

⏑ ⏑ ‒ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

Ηλ.‎ περί μοι ϲτέρνοιϲ| ϲτέρνα πρόϲαψον,‎ 1321 an2

‒ ⏑ ⏑ ‒ ⏑ ⏑

ϲύγγονε φίλτατε·‎ 1322 an

⏑⏑ ‒ ‒ ‒ | ‒ ‒ ⏑ ⏑‒

διὰ γὰρ ζευγνῦ|ϲʼ ἡμᾶϲ πατρίων‎ 1323 an2

⏑ ⏑ ‒ ‒ ‒ | ⏑ ⏑‒ ⏑ ⏑ ‒

μελάθρων μητρὸϲ| φόνιοι κατάραι.‎ 1324 an2

⏑ ⏑ ‒ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

Ορ.‎ βάλε, πρόϲπτυξον| ϲῶμα·‎ θανόντοϲ δʼ‎ 1325 an2

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

ὡϲ ἐπὶ τύμβωι| καταθρήνηϲον.‎ 1326 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

Κα.‎ φεῦ φεῦ·‎ δεινὸν| τόδʼ ἐγηρύϲω‎ 1327 an2

‒ ‒ ⏑ ⏑ ‒

καὶ θεοῖϲι κλύειν.‎ 1328 an

⏑ ⏑ ‒ ‒ ‒ | ‒ ‒ ⏑⏑ ‒

ἔνι γὰρ κἀμοὶ |τοῖϲ τʼ οὐρανίδαιϲ‎ 1329 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒

οἶκτοϲ θνητῶν| πολυμόχθων.‎ 1330 par

‒ ⏑ ⏑ ‒ ⏑ ⏑

〈Ορ.〉‎ οὐκέτι ϲʼ ὄψομαι.‎ 1331 an

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

Ηλ.‎ οὐδʼ ἐγὼ ἐϲ ϲὸν |βλέφαρον πελάϲω.‎ 1332 an2

⏑ ⏑ ‒ ⏑⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

Ορ.‎ τάδε λοίϲθιά μοι |προϲφθέγματά ϲου.‎ 1333 an2

‒ ‒ ⏑ ⏑ ‒

Ηλ.‎ ὦ χαῖρε, πόλιϲ·‎ 1334 an

‒ ⏑ ⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒⏑ ⏑

χαίρετε δʼ ὑμεῖϲ| πολλά, πολίτιδεϲ.‎ 1335-6 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ‒ ‒

Ορ.‎ ὦ πιϲτοτάτη,| ϲτείχειϲ ἤδη;‎ 1337-8 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

Ηλ.‎ ϲτείχω βλέφαρον |τέγγουϲʼ ἁπαλόν.‎ 1339 an2

⏑ ⏑ ‒ ‒ ‒ | ⏑⏑ ⏑ ‒ ‒

Ορ.‎ Πυλάδη, χαίρω|ν ἴθι, νυμφεύου‎ 1340 an2

⏑ ⏑ ‒ ‒ ‒

δέμαϲ Ἠλέκτραϲ.‎ 1341 an

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ⏑ ⏑ ‒

〈Κα.〉‎ τοῖϲδε μελήϲει |γάμοϲ.‎ ἀλλὰ κύναϲ‎ 1342 an2

‒ ⏑ ⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

τάϲδʼ ὑποφεύγων |ϲτεῖχʼ ἐπʼ Ἀθηνῶν·‎ 1343 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

δεινὸν γὰρ ἴχνοϲ| βάλλουϲʼ ἐπὶ ϲοὶ‎ 1344 an2

‒ ⏑ ⏑ ‒ ‒ | ‒ ⏑ ⏑ ‒ ‒

χειροδράκοντεϲ |χρῶτα κελαιναί,‎ 1345 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

δεινῶν ὀδυνῶν| καρπὸν ἔχουϲαι·‎ 1346 an2

‒ ⏑ ⏑ ‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

νὼ δʼ ἐπὶ πόντον |Ϲικελὸν ϲπουδῆι‎ 1347 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

ϲώϲοντε νεῶν| πρώιραϲ ἐνάλουϲ.‎ 1348 an2

⏑⏑ ‒ ⏑ ⏑‒ | ‒ ‒ ⏑ ⏑ ‒

διὰ[^17]‎ δʼ αἰθερίαϲ| ϲτείχοντε πλακὸϲ‎ 1349 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ⏑ ⏑

τοῖϲ μὲν μυϲαροῖ|ϲ οὐκ ἐπαρήγομεν,‎ 1350 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ⏑ ⏑ ‒ ‒

οἷϲιν δʼ ὅϲιον |καὶ τὸ δίκαιον‎ 1351 an2

⏑ ⏑ ‒ ⏑⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

φίλον ἐν βιότωι,| τούτουϲ χαλεπῶν‎ 1352 an2

‒ ‒ ‒ ‒ | ‒ ‒ ‒ ⏑ ⏑

ἐκλύοντεϲ| μόχθων ϲώιζομεν.‎ 1353 an2

‒ ‒ ⏑ ⏑ ‒ | ‒ ‒ ⏑ ⏑ ‒

οὕτωϲ ἀδικεῖν |μηδεὶϲ θελέτω‎ 1354 an2

‒ ⏑ ⏑‒ ‒ | ⏑ ⏑ ‒ ‒ ‒

μηδʼ ἐπιόρκων |μέτα ϲυμπλείτω·‎ 1355 an2

⏑ ⏑ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒

θεὸϲ ὢν θνητοῖ|ϲ ἀγορεύω·‎ 1356 par

‒ ⏑ ⏑ ‒ ‒ | ‒ ‒ ⏑ ⏑ ‒

Χο.‎ χαίρετε·‎ χαίρειν| δʼ ὅϲτιϲ δύναται‎ 1357 an2

‒ ‒ ⏑ ⏑‒ | ‒ ⏑ ⏑ ‒ ‒

καὶ ξυντυχίαι |μή τινι κάμνει‎ 1358 an2

‒ ‒ ‒ ‒ | ⏑ ⏑ ‒ ‒

θνητῶν εὐδαί|μονα πράϲϲει.‎ 1359 par

[^1]: Lourenço just prints “?” here.

[^2]: Lourenço does not scan this line.

[^3]: Lourenço does not scan this line.

[^4]: Lourenço scans only the first six syllables of this line.

[^5]: Lourenço does not scan this line.

[^6]: Lourenço scans only the first four syllables of this line.

[^7]: Lourenço does not scan this line.

[^8]: Lourenço has no 482.

[^9]: Lourenço scans ϲοί as a short syllable and reads the line as —D (‒
    ‒ ‒ ⏑ ‒ ⏑ ⏑ ‒). Perhaps he reads ϲοί as σε (cf. Denniston’s text).

[^10]: Lourenço does not scan this line.

[^11]: See Lourenço, p. 101: “All in all, the disturbing feeling that
    something is amiss cannot be dispelled.”

[^12]: Aichele says 747.

[^13]: Evidently two lines of doc2 missing after 1154.

[^14]: “Impure” iambic metron in second half of line.

[^15]: 1165-1171 are not in Lourenço. Scansion here matches that in
    Denniston 1939.

[^16]: Lourenço does not scan this line.

[^17]: I have removed a period from Diggle’s text here.
