**EURIPIDES *MEDEA***

Prepared by Timothy Moore and Shangwei Deng (nonlyric anapests have not
yet been added and scanned; line numbers for lyrics still need to be
added; abbreviations still need to be matched to ours)

Text: *Euripidis Fabulae*, vol. 1, ed. J. Diggle (Oxford: Clarendon
Press, 1981).\
Scansion of lyrics: Frederico Lourenço, *The Lyric Metres of Euripidean
Drama* (Coimbra: Centro de Estudos Clássicos e Humanisticos da
Universidade de Coimbra, 2011).

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-130**

1-95: ia3

Diggle brackets 38-43

Diggle brackets 87

*96-130: anapests (96-97, 111-114 lyric \[Medea\]; 98-110, 115-130
nonlyric \[Nurse\])*

**PARODOS: 131-213**

*131-138: lyrics (lyric, chorus)*

*anapests (lyric) 131-134*

**Opening section: 131-138**

⏑ ⏑ ‒ ‒ ‒ // ⏑ ⏑ ‒ ⏑ ⏑ ‒

ἔκλυον φωνάν, ἔκλυον δὲ βοὰν anapestic dimeter

‒ ‒ ‒ ‒ // ‒ ⏑ ⏑ ‒ ⏑ ⏑

τᾶς δυστάνου Κολχίδος· οὐδέπω anapestic dimeter||H

‒⏑⏑ ‒ ‒ //⏑ ⏑ ‒ ‒ ‒||

ἤπιος; ἀλλ’, ὦ γεραιά, λέξον. anapestic dimeter||B

‒ ⏑ ⏑‒ ⏑ ⏑‒ ⏑ ⏑ ‒ ⏑ ⏑

ἀμφιπύλου γὰρ ἔσω μελάθρου γόον (135) 4 dactyls

‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑

ἔκλυον, οὐδὲ συνήδομαι, ὦ γύναι, 4 dactyls||H

‒ ⏑ ⏑ ‒ ⏑ ⏑

ἄλγεσι δώματος, 2 dactyls

⏑ ‒ ‒ ⏑⏑‒ ⏑ ‒ ‒

ἐπεί μοι φιλία κέκραται.||| hipponactean

*139-147: anapests (nonlyric, 139-146 \[Nurse\], lyric, 144-147
\[Medea\])*

*148-159 lyrics (lyric, strophe=173-183, chorus) anapests (lyric),
148-150*

STROPHE: 148-159

‒⏑⏑‒ ‒ // ‒ ‒ ‒ ‒

ἄιες, ὦ Ζεῦ καὶ Γᾶ καὶ φῶς, anapestic dimeter

‒ ‒ ‒ ‒//‒ ‒ ‒ ‒

ἀχὰν οἵαν ἁ δύστανος anapestic dimeter

‒ ‒ ‒ ‒||

μέλπει νύμφα; (150) anapestic metron

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

τίς σοί ποτε τᾶς ἀπλάτου 151 hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

κοίτας ἔρος, ὦ ματαία; hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

σπεύσεις θανάτου τελευτάν; hagesichorean

‒ ‒ ⏑ ⏑ ‒ ‒ ||

μηδὲν τόδε λίσσου. reizianum||H

‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

εἰ δὲ σὸς πόσις καινὰ λέχη σεβίζει, (155) cretic + hipponactean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

κείνωι τόδε μὴ χαράσσου· hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

Ζεύς σοι τάδε συνδικήσει. hagesichorean

‒ ⏑ ‒ ‒ ‒ ‒⏑ ⏑ ‒ ⏑ ‒ ‒ ‒|||

μὴ λίαν τάκου δυρομένα σὸν εὐνάταν. cretic + glyonic + spondee[^1]

*160-172: anapests (lyric 160-167 \[Medea\], nonlyric 168-172 \[Nurse\]*

*173-183 lyrics (antistrophe=148-159), anapests (lyric) 173-175*

ANTISTROPHE: 173-183

‒ ⏑ ⏑ ‒ ‒ //‒ ‒⏑⏑ ‒

πῶς ἂν ἐς ὄψιν τὰν ἁμετέραν anapestic dimeter

‒ ‒ ‒ ‒ //‒ ‒ ‒ ‒

ἔλθοι μύθων τ’ αὐδαθέντων anapestic dimeter

‒ ‒ ‒ ‒ ||

δέξαιτ’ ὀμφάν, (175) anapestic metron

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

εἴ πως βαρύθυμον ὀργὰν hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

καὶ λῆμα φρενῶν μεθείη; hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

μήτοι τό γ’ ἐμὸν πρόθυμον hagesichorean

⏑ ‒ ⏑ ⏑ ‒ ‒ ||

φίλοισιν ἀπέστω. reizianum||H

‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ἀλλὰ βᾶσά νιν δεῦρο πόρευσον οἴκων (180) cretic + hipponactean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

ἔξω· φίλα καὶ τάδ’ αὔδα, hagesichorean

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒

σπεύσασά τι πρὶν κακῶσαι hagesichorean

‒ ⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ ‒ |||

τοὺς ἔσω· πένθος γὰρ μεγάλως τόδ’ ὁρμᾶται. cretic + glyconic + spondee

*184-204: anapests (nonlyric, Nurse)*

*205-213: lyrics (lyric, epode)*

EPODE: 205-213

‒ ‒ ‒⏑‒ ⏑ ‒ ⏑ ‒ ⏑‒

ἀχὰν ἄιον πολύστονον γόων 205 mol+2ia[^2]

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒

λιγυρὰ δ’ ἄχεα μογερὰ βοᾶι 206 2ia[^3]

⏑ ‒ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

τὸν ἐν λέχει προδόταν κακόνυμφον· 207 ~\^~e⏑D‒

⏑⏑ ⏑ ‒ ⏑⏑ ⏑ ⏑ ‒ ⏑

θεοκλυτεῖ δ’ ἄδικα παθοῦσα 208 2tr[^4]

‒ ‒ ⏑ ‒ ⏑‒ ⏑⏑ ‒ ⏑ ⏑‒ ⏑

τὰν Ζηνὸς ὁρκίαν Θέμιν, ἅ νιν ἔβασεν 209 ‒e⏑D⏑

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

Ἑλλάδ’ ἐς ἀντίπορον 210 D

⏑ ⏑ ⏑ ⏑ ⏑ ⏑⏑ ‒ ⏑ ‒

δι’ ἅλα νύχιον ἐφ’ ἁλμυρὰν 211 2ia[^5]

‒ ‒ ‒ ⏑ ⏑ ‒ ‒

Πόντου κλῆιδ’ ἀπεράντου. 212 pherecratean[^6]

**FIRST EPISODE: 214**[^7]**-409**

214-356: ia3 (nonlyric)

Diggle brackets 246

Diggle brackets 262

291a: extrametric: φεῦ φεῦ.: Medea

Diggle brackets 304

Diggle brackets 355-356

*357-363: anapests (nonlyric)*

Diggle places 357 before 358

Diggle brackets 361

363-409: ia3 (nonlyric)

385a: extrametric: εἶἑν·: Medea

**FIRST STASIMON: 410-445b**

*410-445b: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 410-420

˘ ¯ ˘ ˘ ¯ ˘˘ ¯ ¯ ¯ ˘ ¯ ¯ ˘D¯e¯

ἄνω ποταμῶν ἱερῶν χωροῦσι παγαί, 410-11

¯ ˘¯ ¯ ¯ ˘ ˘ ¯ ˘ ˘ ¯ e¯D||H

καὶ δίκα καὶ πάντα πάλιν στρέφεται:

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ D¯e

ἀνδράσι μὲν δόλιαι βουλαί, θεῶν δ᾽

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ D¯

οὐκέτι πίστις ἄραρεν.

¯ ˘ ¯ ¯ ¯ ˘ ˘ ¯ ˘˘ ¯ ¯ ¯ ˘ ¯ ¯ e¯D¯e¯||HBa

τὰν δ᾽ ἐμὰν εὔκλειαν ἔχειν βιοτὰν στρέψουσι φᾶμαι: 415-16

¯ ˘ ¯ ¯ ¯ ˘ ¯ ¯ ¯ ˘ ¯ E¯e(Mastronarde)=e‒e‒e‒e

(Lourenço)

ἔρχεται τιμὰ γυναικείῳ γένει:

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ ˘ ¯ ¯ D¯e ba

οὐκέτι δυσκέλαδος φάμα γυναῖκας ἕξει. 420

ANTISTROPHE 1: (421-431

¯ ¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ ¯ ¯D|¯e¯

μοῦσαι δὲ παλαιγενέων λήξουσ᾽ ἀοιδῶν

¯ ˘¯ ¯ ¯ ˘ ˘ ¯ ˘ ˘ ¯ e¯D||Hs

τὰν ἐμὰν ὑμνεῦσαι ἀπιστοσύναν.

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ D¯e

οὐ γὰρ ἐν ἁμετέρᾳ γνώμᾳ λύρας

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ D¯

ὤπασε θέσπιν ἀοιδὰν 425

¯ ˘ ¯ ¯ ¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ ¯ e¯D¯e¯||HBs

Φοῖβος ἁγήτωρ μελέων: ἐπεὶ ἀντάχησ᾽ ἂν ὕμνον

¯ ˘ ¯ ¯ ¯ ˘ ¯ ¯ ¯ ˘ ¯ E¯e=e‒e‒ e||Hs

ἀρσένων γέννᾳ. μακρὸς δ᾽ αἰὼν ἔχει

¯ ˘ ˘ ¯ ˘ ˘ ¯ ¯ ¯ ˘ ¯ ˘ ¯ ¯ D¯e ba

πολλὰ μὲν ἁμετέραν ἀνδρῶν τε μοῖραν εἰπεῖν. 430

STROPHE 2: 432-438b

˘ ¯ ˘ ¯ ¯ ˘ ˘ ¯ ˘ ¯ ¯ ia, aristophanean

σὺ δ᾽ ἐκ μὲν οἴκων πατρίων ἔπλευσας

¯ ˘ ˘ ¯ ˘ ˘¯ ˘ ˘ ¯ ˘ ˘ ¯ ˘ ¯ ¯ praxillean[^8]

μαινομένᾳ κραδίᾳ διδύμους ὁρίσασα Πόντου

¯ ¯ ˘ ˘ ¯ ˘ ¯ telesillean

πέτρας: ἐπὶ δὲ ξένᾳ 435

¯ ¯ ˘ ˘ ¯ ˘ ¯ telesillean∫

ναίεις χθονί, τᾶς ἀνάν-

¯ ¯ ¯ ˘ ˘ ¯ ˘ ¯ glyconic∫

δρου κοίτας ὀλέσασα λέκ-

¯ ˘ ¯ ˘ ˘ ¯ ˘ ¯ glyconic∫

τρον, τάλαινα, φυγὰς δὲ χώ-

¯ ˘ ¯ ˘ ˘ ¯ ¯ pherecratean

ρας ἄτιμος ἐλαύνῃ.

ANTISTROPHE 2: 439-445b

˘ ¯ ˘ ¯ ¯ ˘ ˘ ¯ ˘ ¯ ¯ ia, aristophanean

βέβακε δ᾽ ὅρκων χάρις, οὐδ᾽ ἔτ᾽ αἰδὼς

¯ ˘ ˘ ¯ ˘ ˘ ¯ ˘ ˘ ¯ ˘ ˘ ¯ ˘ ¯ ¯ praxillean

Ἑλλάδι τᾷ μεγάλᾳ μένει, αἰθερία δ᾽ ἀνέπτα. 440-41

¯ ¯ ˘ ˘ ¯ ˘ ¯ telesillean

σοὶ δ᾽ οὔτε πατρὸς δόμοι,

¯ ¯ ˘ ˘ ¯ ˘ ¯ telesillean∫

δύστανε, μεθορμίσα-

¯ ¯ ¯ ˘ ˘ ¯ ˘ ¯ glyconic∫

σθαι μόχθων πάρα, σῶν τε λέκ-

¯ ¯ ¯ ˘ ˘ ¯ ˘ ¯ glyconic∫

τρων ἄλλα βασίλεια κρείσ- 445a

¯ ˘ ¯ ˘ ˘ ¯ ¯ pherecratean

σων δόμοισιν ἐπέστα. 445b

**SECOND EPISODE: 446-626**

446-626: ia3 (nonlyric)

Diggle brackets 467

**SECOND STASIMON: 627-662**

*627-662: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 627-635

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒⏑‒ ⏑D‒e‒e

ἔρωτες ὑπὲρ μὲν ἄγαν ἐλθόντες οὐκ εὐδοξίαν

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ D‒D‒

οὐδ’ ἀρετὰν παρέδωκαν ἀνδράσιν· εἰ δ’ ἅλις ἔλθοι (630)

‒ ⏑ ‒ ‒ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒ e‒D‒

Κύπρις, οὐκ ἄλλα θεὸς εὔχαρις οὕτω.

‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ e‒D‒e‒

μήποτ’, ὦ δέσποιν’, ἐπ’ ἐμοὶ χρυσέων τόξων ἀφείης

‒⏑ ‒ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒ e‒ith[^9]

ἱμέρωι χρίσασ’ ἄφυκτον οἰστόν. (635)

ANTISTROPHE 2: 636-644

‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ ‒ ⏑‒

στέργοι δέ με σωφροσύνα, δώρημα κάλλιστον θεῶν· 636-7 ‒D‒e‒e

‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

μηδέ ποτ’ ἀμφιλόγους ὀργὰς ἀκόρεστά τε νείκη 638-9 D‒D‒

‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

θυμὸν ἐκπλήξασ’ ἑτέροις ἐπὶ λέκτροις 640-1 e‒D‒

‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒

προσβάλοι δεινὰ Κύπρις, ἀπτολέμους δ’ εὐνὰς σεβίζουσ’ 642-3 e‒D‒e‒

‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

ὀξύφρων κρίνοι λέχη γυναικῶν. 644 e‒ith

STROPHE 2: 645-653

‒ ⏑ ⏑ ‒ ‒ ⏑ ⏑ ‒ 2 choriambs

ὦ πατρίς, ὦ δώματα, μὴ (645)

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ aristophanean||

δῆτ’ ἄπολις γενοίμαν

⏑ ⏑ ‒ ⏑ ⏑‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ diomedean

τὸν ἀμηχανίας ἔχουσα δυσπέρατον αἰῶν’, +ithyphallic

‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ hypodochmiac?||[^10]

οἰκτρότατον ἀχέων.

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

θανάτωι θανάτωι πάρος δαμείην 650 T +ba[^11]

‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ wilamowitzian∫

ἁμέραν τάνδ’ ἐξανύσα-

⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ wilamowitzian∫

σα· μόχθων δ’ οὐκ ἄλλος ὕπερ-

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ hipponactean

θεν ἢ γᾶς πατρίας στέρεσθαι.

Antistrophe 2: 654-662

‒ ⏑ ⏑ ‒ ‒ ⏑ ⏑ ‒ 2 choriambs

εἴδομεν, οὐκ ἐξ ἑτέρων

‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ aristophanean

μῦθον ἔχω φράσασθαι· (655)

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ diomedean

σὲ γὰρ οὐ πόλις, οὐ φίλων τις οἰκτιρεῖ παθοῦσαν +ithyphallic

‒ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ hypodoc?||

δεινότατα παθέων.

⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ T+ba

ἀχάριστος ὄλοιθ’ ὅτωι πάρεστιν

‒ ⏑ ‒ ‒ ‒ ⏑ ⏑ ‒ wilamowitzian

μὴ φίλους τιμᾶν καθαρᾶν (660)

⏑ ‒ ‒ ‒ ‒ ⏑ ⏑ ‒ wilamowitzian

ἀνοίξαντα κλῆιδα φρενῶν·

⏑ ‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ hipponactean

ἐμοὶ μὲν φίλος οὔποτ’ ἔσται.

**THIRD EPISODE: 663-823**

663-759: ia3 (nonlyric)

Diggle brackets 725-726

Diggle places 729 after 726

*759-763: anapests (nonlyric)*

764-823: ia3 (nonlyric)

Diggle brackets 785

Diggle brackets 799

**THIRD STASIMON: 824-865**

*824-856: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 824-834

⏑ ‒ ⏑⏑‒⏑ ⏑ ‒⏑‒ ⏑ ‒ ⏑D⏑e

Ἐρεχθεΐδαι τὸ παλαιὸν ὄλβιοι

‒ ⏑‒ ‒ ‒ ⏑⏑ ‒ ⏑⏑‒ e‒D

  καὶ θεῶν παῖδες μακάρων, ἱερᾶς (825)

‒ ‒ ⏑‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑‒ ‒e‒D

   χώρας ἀπορθήτου τ’ ἄπο, φερβόμενοι (827)

‒ ⏑⏑‒ ⏑⏑‒ ‒ ‒⏑⏑‒ ⏑⏑‒ D‒D

  κλεινοτάταν σοφίαν, αἰεὶ διὰ λαμπροτάτου (829)

‒ ‒ ⏑ ‒ ‒ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ‒ ‒e‒D‒

  βαίνοντες ἁβρῶς αἰθέρος, ἔνθα ποθ’ ἁγνὰς (830)

‒⏑⏑‒⏑⏑‒ ‒ ‒ ⏑ ‒ ‒ D‒e‒

  ἐννέα Πιερίδας Μούσας λέγουσι (833)

‒ ‒ ‒ ⏑⏑‒⏑ ‒ ‒ hipponactean[^12]

  ξανθὰν Ἁρμονίαν φυτεῦσαι·

ANTISTROPHE 1: 835-845

‒ ‒ ⏑⏑‒ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒D⏑e

  τοῦ καλλινάου τ’ ἐπὶ Κηφισοῦ ῥοαῖς (835)

‒ ⏑ ‒ ‒ ‒ ⏑⏑ ‒ ⏑⏑‒ e‒D

  τὰν Κύπριν κλήιζουσιν ἀφυσσαμέναν

‒ ‒ ⏑ ⏑ ‒ ‒ ⏑ ⏑ ‒⏑ ⏑‒ ‒e‒D

  χώρας καταπνεῦσαι μετρίας ἀνέμων (838-839)

‒ ⏑ ⏑‒ ‒ ‒ ‒ ‒ ⏑⏑‒ ⏑⏑ ‒ D‒D

  ἡδυπνόους αὔρας· αἰεὶ δ’ ἐπιβαλλομέναν (840)

‒ ‒ ⏑‒ ‒ ‒⏑⏑‒ ⏑ ⏑ ‒ ‒ ‒e‒D‒

   χαίταισιν εὐώδη ῥοδέων πλόκον ἀνθέων

‒ ⏑⏑‒ ⏑ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒ D‒e‒

  τᾶι Σοφίαι παρέδρους πέμπειν Ἔρωτας, (843)

‒ ‒ ‒ ⏑⏑‒ ⏑ ‒ ‒ hipponactean

  παντοίας ἀρετᾶς ξυνεργούς. (845)

STROPHE 2: 846-855

‒ ‒⏑⏑‒ ⏑⏑‒ ‒D

  πῶς οὖν ἱερῶν ποταμῶν

‒ ⏑⏑ ‒⏑‒ dodrans

  ἢ πόλις ἢ φίλων

‒ ⏑ ‒ ⏑ ‒ ‒ ithyphallic

   πόμπιμός σε χώρα

‒ ‒ ⏑⏑‒ ⏑ ‒ ‒ hagesichorean

  τὰν παιδολέτειραν ἕξει,

‒ ‒ ⏑⏑‒⏑ ‒ ‒ hagesichorean||

  τὰν οὐχ ὁσίαν μέταυλον; (850)

‒ ‒ ⏑⏑‒ ‒ ‒ telesillean

  σκέψαι τεκέων πλαγάν,

‒ ‒ ⏑ ⏑ ‒⏑ ‒ ‒ hagesichorean

  σκέψαι φόνον οἷον αἴρηι.

‒ ‒ ⏑ ⏑‒ ⏑ ‒ ‒ hagesichorean

  μή, πρὸς γονάτων σε πάνται

‒ ‒ ⏑⏑‒⏑‒ telesillean

  πάντως ἱκετεύομεν,

‒ ⏑ ⏑ ‒ ‒ adonean

  τέκνα φονεύσηις. (855)

ANTISTROPHE 2: 856-865

⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ⏑D

  πόθεν θράσος
[*†*](http://www.tlg.uci.edu/help/BetaManual/online/P.html)ἢ φρενὸς ἢ

‒ ⏑⏑ ‒ ⏑ ‒ dodrans

   χειρὶ τέκνων σέθεν*†*

‒ ⏑‒⏑ ‒ ‒ ithyphallic

  καρδίαι τε λήψηι

‒ ‒ ⏑ ⏑‒ ⏑ ‒ ‒ hagesichorean

  δεινὰν προσάγουσα τόλμαν;

‒ ‒ ⏑⏑ ‒ ⏑ ‒ ‒ hagesichorean||

  πῶς δ’ ὄμματα προσβαλοῦσα (860)

‒ ‒ ⏑⏑‒ ‒ ‒ telesillean

  τέκνοις ἄδακρυν μοῖραν

‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ ‒ hagesichorean

  σχήσεις φόνου; οὐ δυνάσηι

‒ ‒ ⏑⏑‒ ⏑ ‒ ‒ hagesichorean

  παίδων ἱκετᾶν πιτνόντων

‒ ‒ ⏑ ⏑ ‒ ⏑‒ telesillean

  τέγξαι χέρα φοινίαν

‒ ⏑ ⏑ ‒ ‒ adonean

  τλάμονι θυμῶι.

**FOURTH EPISODE: 866-975**

866-975: ia3 (nonlyric)

Diggle brackets 949

**FOURTH STASIMON: 976-1001**

*976-1001: lyrics (lyric, includes responding stanzas)*

STROPHE 1: 976-982

‒ ‒⏑⏑ ‒⏑ ⏑ ‒ ‒ ‒⏑‒

νῦν ἐλπίδες οὐκέτι μοι παίδων ζόας, 976 ‒D‒e

‒ ⏑‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

οὐκέτι· στείχουσι γὰρ ἐς φόνον ἤδη. 977 e‒D‒

‒ ⏑‒ ‒ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

δέξεται νύμφα χρυσέων ἀναδεσμᾶν 978 e‒D‒

‒ ⏑‒ ‒ ‒⏑ ‒ ‒

δέξεται δύστανος ἄταν· 979 e‒e‒[^13]

‒ ‒ ‒⏑⏑ ‒ ‒ ‒⏑ ‒ ‒

ξανθᾶι δ’ ἀμφὶ κόμαι θήσει τὸν Ἅιδα 980-1 D(contr)‒e‒[^14]

‒ ⏑ ‒ ‒⏑ ‒

κόσμον αὐτὰ χεροῖν. 982 2 cretics

ANTISTROPHE 1: 983-989

‒ ‒ ⏑⏑ ‒ ⏑ ⏑‒ ‒ ‒ ⏑ ‒

πείσει χάρις ἀμβρόσιός τ’ αὐγὰ πέπλον 983 ‒D‒e

‒ ⏑‒ ‒ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ‒

χρυσότευκτόν
[*&lt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)τε[*&gt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)
στέφανον περιθέσθαι· 984 e‒D‒

‒ ⏑ ‒ ‒ ‒⏑ ⏑ ‒ ⏑ ⏑ ‒ ‒

νερτέροις δ’ ἤδη πάρα νυμφοκομήσει. 985 e‒D‒

‒ ⏑ ‒ ‒ ‒ ⏑ ‒ ‒

τοῖον εἰς ἕρκος πεσεῖται 986 e‒e‒

‒ ‒ ‒⏑⏑ ‒ ‒ ‒ ⏑ ‒ ‒

καὶ μοῖραν θανάτου δύστανος· ἄταν δ’ 987 D(contr)‒e‒

‒ ⏑ ‒ ‒ ⏑ ‒

οὐχ ὑπεκφεύξεται. 988 2 cretics

STROPHE 2: 990-995

⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒

σὺ δ’, ὦ τάλαν ὦ κακόνυμφε κηδεμὼν τυράννων, 990-1
erasm[^15]+ithyphallic

‒ ⏑ ‒ ⏑ ‒ ‒

παισὶν οὐ κατειδὼς 992 ithyphallic

⏑⏑ ‒ ⏑⏑ ‒ ⏑ ⏑ ‒ ⏑ ⏑ ‒

ὄλεθρον βιοτᾶι προσάγεις ἀλόχωι 993 A[^16]

⏑ ‒ ⏑⏑ ‒ ⏑⏑ ‒

τε σᾶι στυγερὸν θάνατον. 994 ⏑D

‒ ‒ ⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒

δύστανε, μοίρας ὅσον παροίχηι. 995 iambic + ithyphallic

ANTISTROPHE 2: 996-1001

⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ erasm +ithyphallic

μεταστένομαι δὲ σὸν ἄλγος, ὦ τάλαινα παίδων 996-7

‒ ⏑ ‒ ⏑ ‒ ‒ ithyphallic

μᾶτερ, ἃ φονεύσεις 998

⏑ ⏑ ‒ ⏑⏑‒ ⏑⏑ ‒ ⏑ ⏑ ‒ A

τέκνα νυμφιδίων ἕνεκεν λεχέων, 999

⏑ ‒ ⏑ ⏑ ‒ ⏑⏑ ‒ ⏑D

ἅ σοι προλιπὼν ἀνόμως 1000

‒ ‒ ⏑ ‒ ‒⏑ ‒ ⏑ ‒ ‒ iambic + ithyphallic

ἄλλαι ξυνοικεῖ πόσις συνεύνωι.

**FIFTH EPISODE: 1002-1250**

1002-1080: ia3 (nonlyric)

1004a: extrametric: ἔα·: Medea

Diggle brackets 1006-1007

1007a: extrametric: αἰαῖ.: Medea

Diggle brackets 1055a-1080

1055a: extrametric: \[ἆ ἆ·\]: Medea

*1081-1115: anapests (nonlyric, choral interlude)*

1116-1250: ia3 (nonlyric)

Diggle brackets 1121

Diggle brackets 1221

**FIFTH STASIMON: 1251-1292a**

*1251-1292a: lyrics (lyric, includes responding stanzas)*; ia3
(nonlyric)? 1271-1272 (children)= 1284-1285 (chorus),1277-1278
(children)=1288-1289 (chorus)

STROPHE 1: 1251-1260

⏑‒ ‒⏑ ‒ ‒ ⏑‒ bacchiac, dochmiac

ἰὼ Γᾶ τε καὶ παμφαὴς

‒ ‒ ‒⏑‒ ⏑ ⏑ ⏑ ⏑⏑⏑ ‒ 2 dochmiacs

ἀκτὶς Ἁλίου, κατίδετ’ ἴδετε τὰν

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒⏑‒ 2 dochmiacs

ὀλομέναν γυναῖκα, πρὶν φοινίαν

⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ 2 dochmiacs

τέκνοις προσβαλεῖν χέρ’ αὐτοκτόνον·

‒ ‒ ‒⏑‒ ⏑ ⏑ ⏑ ‒ mol+doc[^17]

σᾶς γὰρ χρυσέας ἀπὸ γονᾶς (1255)

⏑‒ ‒ ⏑‒ ‒ ⏑ ⏑ ‒ ⏑ ‒ 2 dochmiacs

ἔβλαστεν, θεοῦ δ’ αἷμα
[*&lt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)χαμαὶ[*&gt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)
πίτνειν

⏑ ⏑ ⏑ ‒ ⏑ ‒ dochmiac

φόβος ὑπ’ ἀνέρων.

‒ ⏑ ⏑ ‒ ⏑‒ ⏑⏑⏑‒ ⏑ ‒ 2 dochmiacs∫

ἀλλά νιν, ὦ φάος διογενές, κάτειρ-

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ 2 dochmiacs∫

γε κατάπαυσον ἔξελ’ οἴκων τάλαι-

‒ ⏑ ⏑ ‒ ⏑‒ ⏑ ⏑ ⏑‒ ⏑ ‒ 2 dochmiacs

ναν φονίαν τ’ Ἐρινὺν
[*†*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/P.html)ὑπ’
ἀλαστόρων*†*. (1260)

ANTISTROPHE 1: 1261-1270

⏑ ‒ ‒ ⏑ ‒ ‒ ⏑‒ bacchiac, dochmiac

μάταν μόχθος ἔρρει τέκνων,

⏑ ‒ ⏑⏑ ⏑‒ ⏑ ⏑ ⏑ ⏑⏑⏑ ‒ 2 dochmiacs

μάταν ἄρα γένος φίλιον ἔτεκες, ὦ

⏑⏑⏑‒ ⏑ ‒ ⏑ ‒ ‒⏑‒ 2 dochmiacs

κυανεᾶν λιποῦσα Συμπληγάδων

⏑ ‒ ‒⏑ ‒⏑ ‒ ‒ ⏑ ‒ 2 dochmiacs

πετρᾶν ἀξενωτάταν ἐσβολάν.

‒ ‒ ‒⏑ ‒ ⏑ ⏑ ⏑ ‒ mol+ dochmiac

δειλαία, τί σοι φρενοβαρὴς (1265)

⏑ ‒ ‒ ⏑‒ ‒ ⏑ ⏑‒ ⏑ ‒ 2 dochmiacs

χόλος προσπίτνει καὶ ζαμενὴς
[*&lt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)φόνου[*&gt;*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB2.html)

⏑ ⏑ ⏑ ‒ ⏑ ‒ dochmiac

φόνος ἀμείβεται;

⏑ ⏑ ⏑ ‒ ⏑ ‒ ⏑⏑⏑‒ ⏑‒ 2 dochmiacs∫

χαλεπὰ γὰρ βροτοῖς ὁμογενῆ μιά-

⏑ ⏑⏑ ‒⏑ ‒ ⏑ ‒ ‒ ⏑ ‒ 2 dochmiacs∫

σματ’
[*†*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/P.html)ἐπὶ
γαῖαν*†* αὐτοφόνταις ξυνωι-

⏑ ⏑⏑ ‒ ⏑ ‒ ⏑⏑ ⏑ ‒ ⏑ ‒ 2 dochmiacs

δὰ θεόθεν πίτνοντ’ ἐπὶ δόμοις ἄχη. (1270)

Extrametric break (1270a):

ΠΑΙΣ
[*(*](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/SB1.html)ἔσωθεν*)[}](http://www.tlg.uci.edu.libproxy.wustl.edu/help/BetaManual/online/CB.html)*
ἰώ μοι.

STROPHE 2: 1273-1281a

⏑ ‒ ‒ ⏑‒ ⏑‒ ‒ ⏑ ‒

ἀκούεις βοὰν ἀκούεις τέκνων; 1273 2 dochmiacs

⏑‒ ‒⏑ ‒ ⏑ ⏑⏑‒ ⏑‒

ἰὼ τλᾶμον, ὦ κακοτυχὲς γύναι. 1274 2 dochmiacs

‒ ‒ ⏑ ‒ ‒ //‒ ⏑ ‒ ‒ ‒ ⏑ ‒

Πα.^α^ οἴμοι, τί δράσω; ποῖ φύγω μητρὸς χέρας; 1271 ia3

‒ ‒ ⏑ ‒ ⏑//‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

Πα. οὐκ οἶδ’, ἀδελφὲ φίλτατ’· ὀλλύμεσθα γάρ. 1272 ia3

⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

Χο. παρέλθω δόμους; ἀρῆξαι φόνον 1275 2 dochmiacs

⏑ ‒ ‒ ⏑ ‒

δοκεῖ μοι τέκνοις. 1276 dochmiac

‒ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

Πα.^α^ ναί, πρὸς θεῶν, ἀρήξατ’· ἐν δέοντι γάρ. 1277 ia3

‒ ‒ ⏑‒ ‒// ‒ ⏑ ‒ ⏑ ‒ ⏑ ‒

Πα.^β^ ὡς ἐγγὺς ἤδη γ’ ἐσμὲν ἀρκύων ξίφους. 1278 ia3

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ‒ ⏑ ‒ 2 dochmiacs∫

Χο. τάλαιν’, ὡς ἄρ’ ἦσθα πέτρος ἢ σίδα-

⏑ ‒ ‒⏑ ‒ dochmiac

ρος ἅτις τέκνων (1280)

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒⏑‒

ὃν ἔτεκες ἄροτον αὐτόχει- 1281a 2 iambic metra

⏑ ‒ ‒ ⏑ ‒

ρι μοίραι κτενεῖς. 1281b dochmiac

ANTISTROPHE 2: 1282-1292a

⏑‒ ‒ ⏑‒ ⏑‒ ‒ ⏑ ‒

μίαν δὴ κλύω μίαν τῶν πάρος 1282 2 dochmiacs

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑‒ ⏑ ‒

γυναῖκ’ ἐν φίλοις χέρα βαλεῖν τέκνοις, 1283 2 dochmiacs

‒ ‒⏑ ‒ ‒//‒⏑‒ ⏑ ‒ ⏑‒

Ἰνὼ μανεῖσαν ἐκ θεῶν, ὅθ’ ἡ Διὸς 1284 ia3

⏑ ‒ ⏑ ‒ ⏑‒⏑ ‒ ⏑ ‒ ⏑ ‒

δάμαρ νιν ἐξέπεμπε δωμάτων ἄλαις· 1285 ia3

⏑ ‒ ‒ ⏑ ‒ ⏑ ‒ ‒ ⏑ ‒

πίτνει δ’ ἁ τάλαιν’ ἐς ἅλμαν φόνωι 1286 2 dochmiacs

⏑ ‒ ‒ ⏑ ‒

τέκνων δυσσεβεῖ, 1287 dochmiac||H

‒ ‒ ⏑ ‒ ‒ ‒⏑ ‒ ⏑‒ ⏑ ∩

ἀκτῆς ὑπερτείνασα ποντίας πόδα, 1288 ia3||B

⏑ ‒ ⏑ ‒ ‒ //‒ ⏑ ‒ ⏑ ‒ ⏑‒

δυοῖν τε παίδοιν ξυνθανοῦσ’ ἀπόλλυται. 1289 ia3

⏑ ‒ ‒ ⏑ ‒ ⏑ ⏑ ⏑ ‒⏑ ‒

τί δῆτ’ οὐ γένοιτ’ ἂν ἔτι δεινόν; ὦ 1290 2 dochmiacs

⏑ ‒ ‒ ⏑ ‒

γυναικῶν λέχος 1291 dochmiac

⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ⏑ ‒ ⏑‒

πολύπονον, ὅσα βροτοῖς ἔρε- 1292 2ia∫

⏑ ‒ ‒ ⏑ ‒

ξας ἤδη κακά. 1292a dochmiac

**EXODOS: 1293-1419 (=6th Episode)**

1293-1388: ia3 (nonlyric)

Diggle brackets 1316

Diggle brackets 1359

*1389-1419: anapests (nonlyric)*

Diggle brackets 1415-1419

 

[^1]: Mastronarde’s scansion, Lourenço marks “?”. Disc⏑ssing the
    problems on p. 114, he says Mastronarde’s scansion “seems the best
    option.”

[^2]: Mastronarde scans 3^rd^ syllable of verse as short and identifies
    verse as telesillean + iambic metron.

[^3]: Mastronarde identifies verse as ⏑E.

[^4]: Mastronarde identifies verse as E⏑.

[^5]: Mastronarde identifies verse as ⏑E.

[^6]: Mastronarde identifies verse as e~\^~d‒ (=pherecratean).

[^7]: Lourenço and Diggle have no 213.

[^8]: Mastronarde calls this 3 dactyls + aristophanean.

[^9]: Mastronarde calls this E, bacchiac.

[^10]: Mastronarde calls line dodrans.

[^11]: Mastronarde call it diomedean + spondee.

[^12]: Lourenço says, p. 73, that this could be a contracted D+ba.

[^13]: Mastronarde notes “=2tr”.

[^14]: Mastronarde identifies line as e~\^~d‒e‒

[^15]: Mastronarde says ‒D‒.

[^16]: Mastronarde says 2an.

[^17]: Mastronarde call it dochmiac, cretic.
