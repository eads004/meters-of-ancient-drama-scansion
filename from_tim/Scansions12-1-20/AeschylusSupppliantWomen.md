**Aeschylus, *Suppliant Women***

Prepared by Timothy Moore.

Text, scansion, and line numbering: West, M. L., ed. 1990. *Aeschyli
tragoediae: cum incerti poetae Prometheo*. Bibliotheca Scriptorum
Graecorum et Romanorum Teubneriana. Stuttgart: Teubner.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**Parodos: 1-175**

*1-39: anapests (nonlyric)*

*40-175: lyrics (lyric; includes responding stanzas)*

**1^st^ Episode: 176-523**

176-347: ia3 (nonlyric)

*347-437: epirrhematic amoibaian; chorus lyrics (lyric; includes
responding stanzas),* Pelasgus ia3 (nonlyric)

438-523: ia3 (nonlyric)

**1^st^ Stasimon: 524-599**

*524-599: lyrics (lyric; includes responding stanzas)*

**2^nd^ Episode: 600-624**

600-624: ia3 (nonlyric; Danaus reports decision of Argive assembly)

**2^nd^ Stasimon: 625-709**

*625-629: anapests (nonlyric; prayer to Zeus for Argives)*

*630-709: lyrics (lyric, includes responding stanzas)*

**3^rd^ Episode: 710-775**

710-733: ia3 (nonlyric; Danaus reports arrival of Egyptians)

*734-761: epirrhematic amoibaion; chorus in lyrics (lyric, except* ia3
(nonlyric?) at 734-5=741-2, 748-9=755-6), Danaus in is3 (nonlyric)

762-775: ia3 (nonlyric)

**3^rd^ Stasimon: 776-824**

*776-824: lyrics (lyric, includes responding stanzas;* includes ia3s
(nonlyric?), 777-8=785-6, 781/2=789/90, 792-3=800-1)

**Exodos: 825-1073 (=4^th^ Episode)**

*825-871: lyrics (lyric; amoibaion: chorus and Egyptians;* ia3?
(nonlyric?) at 851=863 (Egyptians; lines incomplete; *West proposes
826-826b may be anapests*)

*882-910: epirrhematic amoibaion: chorus in lyrics (lyric, includes
responding stanzas*, except ia3 at *)*, Herald in ia3 (order of 882-884
and 872-875 reversed; also 909-10 come before *908*)

911-965: ia3 (nonlyric; starts with Pelasgus)

*966-979: anapests (nonlyric; chorus bless Pelasgos, ask for Danaos, and
have handmaidens go ahead)*

980-1017: ia3 (nonlyric; Danaus returns and admonishes daughters to be
chaste)

*1018-1073: lyrics (lyric; includes responding stanzas; choruses of
Danaids and Argives:*
