Sophocles *Antigone* Scansion

Prepared by Timothy Moore

Text and Scansion: *Sophocles, Antigone*, ed. Mark Griffith, Cambridge
University Press, 1999.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-99**

1-99: ia3 (nonlyric)

**PARODOS: 100-161**

*100-161: lyrics (lyric, with responding stanzas) *

*alternating with anapests (nonlyric): 110-116=128-133, 141-146=155-161
(last announces entrance of Creon)*

**FIRST EPISODE: 162-331**

162-331: ia3 (nonlyric)

322a: extrametric: φεῦ: Guard

**FIRST STASIMON: 332-383**

*332-375: lyrics (lyric, with responding stanzas)*

*376-383: anapests (nonlyric, chorus announces Antigone)*

**SECOND EPISODE: 384-581**

384-525: ia3 (nonlyric)

*526-530: anapests (nonlyric, chorus announces Ismene)*

531-581: ia3 (nonlyric)

**SECOND STASIMON: 582-630**

*582-625: lyrics (lyric, includes responding stanzas)*

*626-630: anapests (nonlyric, chorus announces Haemon)*

**THIRD EPISODE: 631-780**

631-780: ia3 (nonlyric)

Griffith rearranges lines between 663 and 671

**THIRD STASIMON: 781-805**

*781-800: lyrics (lyric, includes responding stanzas)*

*801-805: anapests (nonlyric, chorus announces Antigone)*

**FOURTH EPISODE: 806**[^1]**-943**

*806-882: Epirrhematic Amoibaion: Antigone lyrics (lyric, with
responding stanzas), chorus anapests (nonlyric) 817-822, 834-838, then
both lyrics (lyric, with responding stanzas) from 839 on.*

883-928: ia3 (nonlyric)

*929-943: anapests (nonlyric, Antigone goes off)*

**FOURTH STASIMON: 944-987**

*944-987: lyrics (lyric, includes responding stanzas)*

**FIFTH EPISODE: 988-1114**

988-1114: ia3 (nonlyric)

1047a: extrametric: φεῦ: Teiresias

**FIFTH STASIMON: 1115-1154**

*1115-1154: lyrics (lyric, includes responding stanzas)*

**EXODOS: 1155-1353 (=6^th^ episode)**

1155-1256: ia3 (nonlyric)

*1257-1260: anapests (nonlyric, chorus announces and blames Creon)*

*1261-1346: Epirrhematic amoibaion (with responding stanzas); Creon in
lyrics (lyric)* and ia3s (nonlyric)*,* chorus in ia3s (nonlyric),
Messenger in ia3s (nonlyrics)

*1347-1353: anapests (nonlyric, choral tag)*

[^1]: Aichele says 805.
