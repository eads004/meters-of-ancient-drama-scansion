Sophocles *Ajax*: Outline and Scansion

Prepared by Timothy Moore

Text and Scansion: *Sophocles, Aias*, ed. P.J. Finglass, Cambridge
University Press, 2011

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-133**

1-133: ia3 (nonlyric)

**PARODOS: 134-200**

*134-171 anapests (nonlyric)*

*172-200: lyrics (lyric, including responding stanzas)*

**FIRST EPISODE: 201-595**

*201-220: anapests (nonlyric)*

*221-262: Epirrematic Amoibaion: chorus lyrics (lyric, with responding
stanzas), Tecmessa anapests (nonlyric)*

STROPHE: 221-232

*233-244: anapests (nonlyric, Tecmessa)*

ANTISTROPHE: 245-256

*257-262: anapests (nonlyric, Tecmessa)*

263-344: ia2 (nonlyric)

333: extrametric: ἰώ μοί μοί Ajax

336: extrametric: ἰώ μοί μοί Ajax

339: extrametric: ἰώ παῖ παῖ Ajax

*348-429: Epirrhematic Amoibaion: Ajax lyrics (lyric, with responding
stanzas)*, except ia3 367=382 (lyric?), 369=384 (nonlyric?); chorus and
Tecmessa ia3 (nonlyric)

STROPHE 1: 348-353

354-355: ia3 (nonlyric, chorus)

ANTISTROPHE 1: 356-361

362-363: ia3 (nonlyric, chorus)

STROPHE 2: 364-367 and 372-376

368-371: ia3 (Tecmessa, Ajax)

370: extrametric: αἰαῖ, αἰ αῖ: Ajax

ANTISTROPHE 2: 379-382 and 387-391

383-386: ia3 (Chorus, Ajax)

385: extrametric ἰώ μοί μοί: Ajax

392-393: ia3 (nonlyric, Tecmessa)

STROPHE 3: 394a-409

410-411: ia3 (nonlyric, Tecmessa)

ANTISTROPHE 3: 412a-427

428-429: ia3 (nonlyric, chorus)

430-595: ia3 (nonlyric)

Finglass brackets 554b

Finglass brackets 571

Finglass assumes a lacuna that makes 573 two verses

**FIRST STASIMON: 596-645**

*596-645: lyrics (lyric, includes responding stanzas)*

**SECOND EPISODE: 646-692**

646-692: ia3 (nonlyric, Ajax’s Trugrede)

**SECOND STASIMON: 693-718**

*693-718: lyrics (lyric, including responding stanzas)*

**THIRD EPISODE: 719-865**

719-865: ia3 (nonlyric)

737a: extrametric: ἰοὺ ἰού: Messenger

Finglass brackets 812

Chorus leaves stage after 814

**EPIPARODOS: 866-890**

*866-878: lyrics (lyric, astrophic)* except ia3 (nonlyric) 869, 872,
874, 876-878 (half choruses)

*879-890: lyrics (lyric: strophe with antistrophe in 4^th^ episode)*

STROPHE: 879-890

**FOURTH EPISODE: 891-1184**

*891-960: Epirrhematic Amoibaion: Chorus and Tecmessa both mix lyrics
(lyric: includes 1 stanza responding to what is before) and* ia3
(nonlyric)

961-1162: ia3 (nonlyric)

974: extrametric: ἰώ μοί μοί: Teucer

Antilabe: 981-983

1002: extrametric: οἴμοι: Teucer

Finglass brackets 1028-1039

Finglass brackets 1061

*1163-1167: anapests (nonlyric, chorus)*

1168-1184: ia3 (nonlyric)

**THIRD STASIMON: 1185-1222**

*1185-1222: lyrics (lyric, includes responding stanzas)*

**EXODOS: 1223-1420 (=4^th^ episode)**

1223-1401: ia3 (nonlyric)

*1402-1420: anapests (nonlyric; Teucer then choral tag)*
