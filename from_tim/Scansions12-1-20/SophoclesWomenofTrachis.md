Sophocles *Women of Trachis* Scansion

Prepared by Timothy Moore

Text: *Sophoclis Fabulae*, edd. H. Lloyd-Jones and N.G. Wilson, Oxford:
Clarendon Press, 1990.

Episode structure: Aichele, Klaus. 1971. “Das Epeisodion.” In *Die
Bauformen der griechischen Tragödie*, edited by Walter Jens, 47–83.
Munich: Fink. Pp. 50-51.

**PROLOGOS: 1-93**

1-93: ia3 (nonlyric)

**PARODOS: 94-140**

*94-140: lyrics (lyric, includes responding stanzas)*

**FIRST EPISODE: 141-496**

141-204: ia3 (nonlyric)

*205-224: lyrics (lyric, astrophic, chorus rejoices)*, ia3 (lyric?) 207,
217

225-496: ia3 (nonlyric)

409: antilabe

418: antilabe

424a: extrametric: ναί: Lichas

**FIRST STASIMON: 497-530**

*497-530: lyrics (lyric, includes responding stanzas)*

**SECOND EPISODE: 531-632**

531-632: ia3 (nonlyric)

**SECOND STASIMON: 633-662**

*633-662: lyrics (lyric, includes responding stanzas)*

**THIRD EPISODE: 663-820**

663-820: ia3 (nonlyric)

Lloyd-Jones and Wilson bracket 884

**THIRD STASIMON: 821-861**

*821-861: lyrics (lyric, with responding stanzas);* ia3 (lyric?)
824-5=834-5

**FOURTH EPISODE: 862**[^1]**-946**

862: ia\^: ἰώ μοι: Nurse (added by Meineke)

863-864: ia3 (nonlyric)

865: ia\^: τί φημι;: Chorus

866-867: ia3 (nonlyric)

868: ia\^: ξύνες δὲ: chorus

869-879: ia3

876-877: antilabe

879: antilabe

*880-895: Epirrhematic Amoibaion: chorus lyrics (lyric, astrophic)*, and
ia3 (lyric?) 888; Nurse ia3 (nonlyric)*, and lyric (892: in antilabe)*

896-946: ia3 (nonlyric)

Lloyd-Jones and Wilson bracket 911

**FOURTH STASIMON: 947-970**

*947-970: lyrics (lyric, includes responding stanzas)*

**EXODOS: 971-1278 (=5^th^ episode)**

*971-1003: anapests (nonlyric)*

*1004-1043: Lyrics*; *Heracles lyrics (lyric, includes responding
stanzas), including da6 (lyric?); Presbys and Hyllus da6 (lyric?)*

1044-1080: ia3 (nonlyric)

1081: dochmiac (lyric): αἰαῖ ὦ τάλας: Heracles

1081: extrametric: αἰαῖ

1082-1084: ia3 (nonlyric)

*1085-1086: anapests (lyric?)*

1087-1258: ia3 (nonlyric)

*1259-1278: anapests (nonlyric)*

[^1]: Aichele says 863.
