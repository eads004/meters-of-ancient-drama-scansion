#!/home/spenteco/anaconda2/envs/py3/bin/python

import glob, re, json
from io import StringIO
from lxml import etree
from Greek_Prosody.syllables import syllabify_moore_rules
from Greek_Prosody.characters import *

class HypotacticScansion:
    
    def __init__(self, 
                    html_folder=None, 
                    tim_folder=None, 
                    json_file=None, 
                    print_output=False,
                    drop_AESCHYLUS=False):
        
        self.word_dictionary = {}
            
        temp = []

        if html_folder != None:

            for p in glob.glob(html_folder + '*.html'):
                
                play = p.split('/')[-1].split('.')[0]
                
                if drop_AESCHYLUS == True:
                    if play in ['persians', 'prometheus', 'seven']:
                        continue
                
                #if print_output:
                #    print()
                #    print(play, p)
                #    print()
                
                parser = etree.HTMLParser()
                
                html = open(p, 'r', encoding='utf-8').read()
                
                tree = etree.parse(StringIO(html), parser)
                
                for div in tree.xpath('//div'):
                    
                    if 'line' in div.get('class'):
                        
                        meter = div.get('data-metre')
                        line_n = div.get('data-number')
                        
                        #if line_n in ['976', '1023']:
                        #    pass
                        #else:
                        #    continue
                        
                        words = []

                        for span in div.xpath('descendant::span[@class="word"]'):
                            words.append(re.sub('\s+', '', ''.join(span.itertext())))

                        line_text = ' '.join(words)
                        line_text = line_text.replace("’", "'")
                        line_text = line_text.replace("‘", "'")
                        line_text = line_text.replace("nbsp;", "")
                        line_text = line_text.replace("&nbsp", "")
                        line_text = line_text.replace("&nbs", "")
                        line_text = line_text.replace("&nb", "")
                        line_text = line_text.replace('"', "")
                        
                        if len(re.sub('\s+', '', line_text).strip()) < 10:
                            continue
                        
                        syllables = []
                        
                        syllables = None
                        try:
                            syllables = syllabify_moore_rules(line_text, NO_ERRORS=True)
                        except IndexError:
                            #if print_output:
                            #    print('syllable_fault', play, line_n, line_text)
                            pass
                            
                        #if print_output:
                        #    print('ok?', play, line_n, line_text, syllables)
                    
                        lengths = []
                        
                        for span in div.xpath('descendant::span'):
                            if span.get('class') != None and 'syll' in span.get('class'):
                                if 'short' in span.get('class'):
                                    lengths.append('S')
                                elif 'long' in span.get('class'):
                                    lengths.append('L')
                                else:
                                    lengths.append('X')
                                
                        if syllables != None and len(syllables) == len(lengths):
                                
                            temp.append({'play': play, 
                                            'meter': meter,
                                            'line_n': line_n,
                                            'line_text': line_text,
                                            'syllables': ' '.join(syllables),
                                            'n_syllables': len(syllables),
                                            'lengths': ' '.join(lengths),
                                            'n_lengths': len(lengths)})

        if tim_folder != None:            
            
            for p in glob.glob(tim_folder + '*.json'):
                
                play = p.split('/')[-1].split('.')[0]
                
                #if print_output:
                #    print()
                #    print(play, p)
                #    print()
                    
                tim_data = json.load(open(p, 'r', encoding='utf-8'))
                
                line_number = 0
                
                for line in tim_data:
                    
                    line_number += 1
                        
                    meter = ''
                    line_n = str(line_number)
                        
                    line_text = line[1]
                        
                    if len(re.sub('\s+', '', line_text).strip()) < 10:
                        continue
                    if '⟨' in line_text or '⟩' in line_text or '—' in line_text:
                        continue
                        
                    syllables = None
                    try:
                        syllables = syllabify_moore_rules(line_text, NO_ERRORS=True)
                    except IndexError:
                        #if print_output:
                        #    print('syllable_fault', play, line_n, line_text)
                        pass
                    
                    lengths = line[0].split(' ')
                                
                    if syllables != None and len(syllables) == len(lengths):
                            
                        temp.append({'play': play, 
                                        'meter': meter,
                                        'line_n': line_n,
                                        'line_text': line_text,
                                        'syllables': ' '.join(syllables),
                                        'n_syllables': len(syllables),
                                        'lengths': ' '.join(lengths),
                                        'n_lengths': len(lengths)})
            
        #if print_output:
        #    print()     
        #    print('len(temp)', len(temp))
        #    print()

        if html_folder != None or tim_folder != None:
                
            for r in temp:
                
                #if print_output:
                #    print()
                #    print('r', r['line_text'])

                line_syllable_results, syllable_word_mapping = \
                                        self.map_syllables_and_words(r['line_text'],
                                                                        r['syllables'],
                                                                        r['lengths'],
                                                                        print_output=print_output)
                    
                #if print_output:
                #    print()
                #    print('\t', 'line_syllable_results', line_syllable_results)
                #    print()
                #    print('\t', 'syllable_word_mapping', syllable_word_mapping)
                   
                words_and_word_n = {}
                max_word_number = -1
                
                for mapping in syllable_word_mapping:
                    for word in mapping['to_words']:
                        if word['word_number'] > max_word_number:
                            max_word_number = word['word_number']
                        if word['w'] not in words_and_word_n:
                            words_and_word_n[word['w']] = []
                        words_and_word_n[word['w']].append(word['word_number'])
                    
                #if print_output:
                #    print()
                #    print('\t', 'words_and_word_n', words_and_word_n)
                #    print('\t', 'max_word_number', max_word_number)
                #    print()
                    
                for word, mappings in line_syllable_results.items():
                    
                    if max_word_number in words_and_word_n[word]:
                        #if print_output:
                        #    print('\t', 'DROPPING', word)
                        continue
                    
                    if word not in self.word_dictionary:
                        self.word_dictionary[word] = []
                    for m in mappings:
                        self.word_dictionary[word].append(m)
                    
                    #if print_output:
                    #    print()
                    #    print('\t', word, len(mappings), mappings)
                    
            f = open(json_file, 'w', encoding='utf-8')
            f.write(json.dumps(self.word_dictionary, indent=4, ensure_ascii=False))
            f.close()
                    
            
        elif json_file != None:
                    
            f = open(json_file, 'r', encoding='utf-8')
            self.word_dictionary = json.load(f)
            f.close()
            
        #if print_output:
            
        #    print()
        #    print('len(self.word_dictionary)', len(self.word_dictionary))
            
        #    for k in list(self.word_dictionary.keys())[:5]:
        #        print()
        #        print(k, self.word_dictionary[k])

    def map_syllables_and_words(self, 
                                line_text, 
                                syllables, 
                                lengths, 
                                print_output=False, 
                                print_errors=True):
                        
        if print_output:
            print()
            print('line_text', line_text)
            print('syllables', syllables)
            print('lengths', lengths)
        
        words = line_text.split(' ')
        
        words_string = ''
        words_offsets = []
        
        a = 0
        for w in words:
            words_string += w
            words_offsets.append([w, a, a + len(w) - 1])
            a += len(w)
        
        syllables = syllables.split(' ')
        lengths = lengths.split(' ')
        
        syllables_offsets = []
        
        a = 0
        for s in syllables:
            starting_pos = words_string.find(s, a)
            ending_pos = starting_pos + len(s) - 1
            syllables_offsets.append([s, starting_pos ,ending_pos])
            a += len(s)
            
        if print_output:
            print()
            print('words_offsets', words_offsets)
            print()
            print('syllables_offsets', syllables_offsets)
            print()
            
        syllable_word_mapping = []
        last_word_mapped = 0
        
        for sn, s in enumerate(syllables_offsets):
            
            mapping_01 = {'syllable': s[0], 'length': lengths[sn], 'to_words': []}
            
            for word_number, w in enumerate(words_offsets):
                if s[1] <= w[1] and s[2] >= w[1] or \
                    s[1] >= w[1] and s[2] <= w[2] or \
                    s[1] <= w[2] and s[2] >= w[2]:
                
                    mapping_01['to_words'].append({'w': w[0], 'word_number': word_number})
            
            basic_syllable = basic_chars(s[0])
            
            syllable_vowel = re.sub('[^' + ''.join(VOWELS) + ']', '', basic_chars(basic_syllable))
            
            basic_words = []
            for w in mapping_01['to_words']:
                basic_words.append(basic_chars(w['w']))
                
            combined_basic_words = ''.join(basic_words)
                    
            syllable_starting_location = combined_basic_words.find(basic_syllable)
            syllable_ending_location = syllable_starting_location + len(basic_syllable) - 1
            
            vowel_starting_location = combined_basic_words.find(syllable_vowel,
                                                                syllable_starting_location, 
                                                                syllable_ending_location + 1)
            
            final_to_word = None
            
            if print_output:
                print()
                
            running_word_length = 0
            for w in mapping_01['to_words']:
                
                if print_output:
                    print('\t', 'vowel_starting_location', vowel_starting_location,
                                'running_word_length', running_word_length,
                                'len(w[\'w\'])', len(w['w']))
                            
                if vowel_starting_location < running_word_length + len(w['w']):
                    final_to_word = w
                    break
                else:
                    running_word_length = running_word_length + len(w['w'])
            
            mapping_02 = {'syllable': s[0], 'length': lengths[sn], 'to_words': [final_to_word]}
            
            word_syllables = syllabify_moore_rules(mapping_02['to_words'][0]['w'], NO_ERRORS=True)
                    
            for syllable_number, word_syllable in enumerate(word_syllables):
                if syllable_vowel in basic_chars(word_syllable):
                    mapping_02['to_words'][0]['syllable_number'] = syllable_number
                    break
                    
            syllable_word_mapping.append(mapping_02)
            
            if print_output:
                print()
                print('mapping_01', mapping_01)
                print('len(mapping_01[\'to_words\'])', len(mapping_01['to_words']))
                print('s', s, 'basic_syllable', basic_syllable, 'syllable_vowel', syllable_vowel)
                print('basic_words', basic_words)
                print('combined_basic_words', combined_basic_words)
                print('syllable_starting_location', syllable_starting_location,
                        'syllable_ending_location', syllable_ending_location)
                print('vowel_starting_location', vowel_starting_location)
                print('word_syllables', word_syllables)
                print('mapping_02', mapping_02)
        
        line_syllable_results = {}
        
        for s in syllable_word_mapping:
            
            if print_output:
                print()
                print('syllable_word_mapping', s) 
            
            if len(s['to_words']) > 0:
            
                word = s['to_words'][0]['w']
                
                if 'syllable_number' not in s['to_words'][0]:
                    line_syllable_results = {}
                    if print_output:
                        print()
                        print('ERROR', 'missing syllable_number', s)
                        print()
                    break
                
                if s['to_words'][0]['syllable_number'] == 0:
                    if word not in line_syllable_results:
                        line_syllable_results[word] = [[]]
                    else:
                        line_syllable_results[word].append([])
                        
                try:
                    line_syllable_results[word][-1].append({
                                        'syllable_n': s['to_words'][0]['syllable_number'],
                                        'syllable': s['syllable'], 
                                        'length': s['length']})
                except KeyError:
                    line_syllable_results = {}
                    if print_output:
                        print()
                        print('ERROR', '> 1 word rule fault (b)', s)
                        print()
            else:
                if print_output:
                    print()
                    print('ERROR', 'no to words?', s)
                    print()
                pass
                
        return line_syllable_results, syllable_word_mapping

    def lookup_syllable_lengths(self, lookup_syllable, lookup_word, lookup_syllable_number):
        
        results = []
        
        if lookup_word in self.word_dictionary:
            
            for word in self.word_dictionary[lookup_word]:
                for s in word:
                    if s['syllable_n'] == lookup_syllable_number and \
                        s['syllable'] == lookup_syllable:
                        results.append(s['length'])
        
        return results

if __name__ == "__main__":
    
    #hypotactic_scansion = HypotacticScansion(
    #    html_folder = '/home/spenteco/1/moore_February_2021/from_hypotactic/',
    #    json_file = '/home/spenteco/1/moore_February_2021/test_inputs/' + \
    #                    'NO.AESCHYLUS.hypotactic_word_dictionary.json', print_output=False) 
        
    #print(len(hypotactic_scansion.word_dictionary))
    
    #hypotactic_scansion = HypotacticScansion(
    #    json_file = '/home/spenteco/1/moore_February_2021/test_inputs/' + \
    #                    'NO.AESCHYLUS.hypotactic_word_dictionary.json') 
        
    #print(len(hypotactic_scansion.word_dictionary))
    
    hc = HypotacticScansion()
    
    # THIS ONE HAS A TWO-WORD SYLLABLE
    line_syllable_results, syllable_word_mapping = \
        hc.map_syllables_and_words('πριάμου τε πατρός, ὅς μʼ, ἐπεὶ φρυγῶν πόλιν', 
                                   'πρι ά μου τε πα τρό ς,ὅς μʼ,ἐ πεὶ φρυ γῶν πό λιν', 
                                   'X X L S X S L S L X L S L',
                                   print_output=False)
                                   
    print()
    print('--------------------------------------------------')
    print()
    print('line_syllable_results', line_syllable_results)
    print()
    print('syllable_word_mapping', syllable_word_mapping)
    
    # THIS ONE HAS A THREE-WORD SYLLABLE
    line_syllable_results, syllable_word_mapping = \
        hc.map_syllables_and_words('νεώτατος δʼ ἦ Ππριαμιδῶν, ὃ καί με γῆς', 
                                   ' '.join(['νε', 'ώ', 'τα', 'τος', 'δʼἦΠ', 
                                            'πρι', 'α', 'μι', 'δῶ', 'ν,ὃ', 
                                            'καί', 'με', 'γῆς']), 
                                   ' '.join(['S', 'L', 'X', 'L', 'L', 'X', 
                                            'X', 'X', 'L', 'S', 'L', 'S', 'L']),
                                   print_output=False)
                                   
    print()
    print('--------------------------------------------------')
    print()
    print('line_syllable_results', line_syllable_results)
    print()
    print('syllable_word_mapping', syllable_word_mapping)
    
